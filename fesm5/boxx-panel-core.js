import { __decorate, __awaiter, __generator, __param, __assign, __spread } from 'tslib';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Input, Component, InjectionToken, Inject, Injectable, ɵɵdefineInjectable, ɵɵinject, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Platform, PopoverController, LoadingController, AlertController, ActionSheetController, IonicModule } from '@ionic/angular';
import { createEffect, ofType, Actions, EffectsModule } from '@ngrx/effects';
import { createAction, props, createReducer, on, createFeatureSelector, createSelector, Store, StoreModule } from '@ngrx/store';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { Subject, of } from 'rxjs';
import { File } from '@ionic-native/file/ngx';
import { takeUntil, map, catchError, switchMap } from 'rxjs/operators';
import { AbstractAppConfigService, APP_CONFIG_SERVICE } from '@boxx/core';
import { LeadModel } from '@boxx/messages-core';
import { EventModel } from '@boxx/events-core';
import { ContactModel } from '@boxx/contacts-core';

var ContactChartWidgetComponent = /** @class */ (function () {
    function ContactChartWidgetComponent() {
    }
    ContactChartWidgetComponent.prototype.ngOnInit = function () { };
    __decorate([
        Input()
    ], ContactChartWidgetComponent.prototype, "stats", void 0);
    __decorate([
        Input()
    ], ContactChartWidgetComponent.prototype, "labels", void 0);
    ContactChartWidgetComponent = __decorate([
        Component({
            selector: 'boxx-contact-chart-widget',
            template: "<ion-grid class=\"ion-margin-bottom ion-no-padding\">\n    <ion-row>\n        <ion-col size=\"4\" id=\"contacts-chart-col\">\n            <div id=\"contacts-chart-counter\">\n                <div>\n                    <h3>{{stats.total}}</h3>\n                    {{labels.contacts}}\n                </div>\n            </div>\n\n            <div id=\"contacts-chart-container\">\n                <ng-content></ng-content>\n            </div>\n        </ion-col>\n\n        <ion-col size=\"8\" id=\"contacts-stats-col\">\n            <ion-item>\n                <ion-label>\n                    <p>\n                        <span class=\"contact-bullet bullet1\"></span>\n                        {{labels.not_specified}}\n                    </p>\n                </ion-label>\n                <ion-note slot=\"end\" class=\"ion-no-margin\">{{stats.not_specified}}%</ion-note>\n            </ion-item>\n\n            <ion-item>\n                <ion-label>\n                    <p>\n                        <span class=\"contact-bullet bullet2\"></span>\n                        {{labels.prospect}}\n                    </p>\n                </ion-label>\n                <ion-note slot=\"end\" class=\"ion-no-margin\">{{stats.prospect}}%</ion-note>\n            </ion-item>\n\n            <ion-item>\n                <ion-label>\n                    <p>\n                        <span class=\"contact-bullet bullet3\"></span>\n                        {{labels.client}}\n                    </p>\n                </ion-label>\n                <ion-note slot=\"end\" class=\"ion-no-margin\">{{stats.client}}%</ion-note>\n            </ion-item>\n        </ion-col>\n    </ion-row>\n</ion-grid>",
            styles: ["#contacts-chart-col{padding:0}#contacts-stats-col{padding-left:16px}#contacts-chart-container{position:relative;width:69vw;margin-left:-20vw}#contacts-chart-counter{height:100%;position:absolute;z-index:2;text-align:center;align-items:center;display:-ms-grid;display:grid;width:100%}#contacts-chart-counter h3{margin:0}.contact-bullet{width:10px;height:12px;display:inline-flex;margin-right:4px;border-radius:3px}.contact-bullet.bullet1{background:var(--ion-color-primary,red)}.contact-bullet.bullet2{background:var(--ion-color-secondary,green)}.contact-bullet.bullet3{background:var(--ion-color-tertiary,#00f)}"]
        })
    ], ContactChartWidgetComponent);
    return ContactChartWidgetComponent;
}());

var KpiCellComponent = /** @class */ (function () {
    function KpiCellComponent() {
    }
    KpiCellComponent.prototype.ngOnInit = function () { };
    __decorate([
        Input()
    ], KpiCellComponent.prototype, "className", void 0);
    __decorate([
        Input()
    ], KpiCellComponent.prototype, "iconName", void 0);
    __decorate([
        Input()
    ], KpiCellComponent.prototype, "iconSrc", void 0);
    __decorate([
        Input()
    ], KpiCellComponent.prototype, "label", void 0);
    __decorate([
        Input()
    ], KpiCellComponent.prototype, "value", void 0);
    KpiCellComponent = __decorate([
        Component({
            selector: 'boxx-kpi-cell',
            template: "<!-- KPI CELL -->\n<div class=\"kpi {{className}}\">\n    <div class=\"kpi-icon\">\n        <ion-icon name=\"{{iconName}}\" src=\"{{iconSrc}}\" [hidden]=\"!iconName && !iconSrc\"></ion-icon>\n    </div>\n    <div class=\"kpi-label\">\n        {{label}}\n    </div>\n    <div class=\"kpi-counter\">\n        <div class=\"value\">{{value}}</div>\n    </div>\n</div>",
            styles: [":host{display:contents}:host .kpi{text-align:center;min-width:120px;max-width:100%;min-height:125px;max-height:300px;padding:10px;box-shadow:#d3d3d3 0 0 4px;border-radius:29px;background-color:#fff;position:relative;display:-ms-grid;display:grid}:host .kpi ion-icon{font-size:x-large;display:flex;margin:auto}"]
        })
    ], KpiCellComponent);
    return KpiCellComponent;
}());

var MainHeaderComponent = /** @class */ (function () {
    function MainHeaderComponent() {
    }
    MainHeaderComponent.prototype.ngOnInit = function () { };
    MainHeaderComponent = __decorate([
        Component({
            selector: 'boxx-main-header',
            template: "<div id=\"panel-header\">\n    <ion-row id=\"panel-header-info\">\n        <ion-col size=\"10\">\n            <ng-content select=\"ion-label\"></ng-content>\n        </ion-col>\n        <ion-col size=\"2\" id=\"header-right-col\">\n            <ng-content select=\"boxx-qr-component\"></ng-content>\n        </ion-col>\n    </ion-row>\n    <ng-content></ng-content>\n</div>",
            styles: ["#panel-header{position:relative;height:160px;border-bottom-left-radius:50% 50%;border-bottom-right-radius:50% 50%;width:120%;margin-top:-1px;margin-left:-10%;padding-left:10%;padding-right:10%;background-color:var(--ion-color-primary,#fff);color:var(--ion-color-primary-contrast,#000);border-left:6px solid var(--ion-color-secondary,#000);border-bottom:6px solid var(--ion-color-secondary,#000);border-right:6px solid var(--ion-color-secondary,#000)}#panel-header-info{font-size:12px;padding:8px}#panel-header-info ion-col{padding:0}#header-right-col{padding:1px;font-size:2.7em;text-align:right}"]
        })
    ], MainHeaderComponent);
    return MainHeaderComponent;
}());

var PanelWidgetComponent = /** @class */ (function () {
    function PanelWidgetComponent() {
    }
    PanelWidgetComponent.prototype.ngOnInit = function () { };
    __decorate([
        Input()
    ], PanelWidgetComponent.prototype, "title", void 0);
    PanelWidgetComponent = __decorate([
        Component({
            selector: 'boxx-panel-widget',
            template: "<div class=\"boxx-panel-widget\">\n    <ion-item lines=\"none\" [hidden]=\"!title\">\n        <ion-label class=\"widget-title\">{{title}}</ion-label>\n\n        <ion-note slot=\"end\">\n            <ng-content select=\"div[forwardLink]\"></ng-content>\n        </ion-note>\n    </ion-item>\n\n    <ng-content></ng-content>\n</div>",
            styles: [":host .boxx-panel-widget{margin-bottom:16px}:host .widget-title{font-size:larger;font-weight:600}"]
        })
    ], PanelWidgetComponent);
    return PanelWidgetComponent;
}());

var QrPopoverComponent = /** @class */ (function () {
    function QrPopoverComponent(platform, popoverCtrl, file, loaderCtrl, alertCtrl, actionSheetController, translate) {
        var _this = this;
        this.platform = platform;
        this.popoverCtrl = popoverCtrl;
        this.file = file;
        this.loaderCtrl = loaderCtrl;
        this.alertCtrl = alertCtrl;
        this.actionSheetController = actionSheetController;
        this.translate = translate;
        this.isCordovaOrCapacitor = false;
        this.elementType = 'url';
        this.destroyed$ = new Subject();
        this.translate.get(['GENERAL', 'QR'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe(function (t) { return _this.translations = t; });
        this.platform.ready().then(function () {
            _this.isCordovaOrCapacitor = _this.platform.is('cordova') || _this.platform.is('capacitor');
            if (_this.platform.is('android')) {
                _this.isAndroid = true;
                _this.folderpath = _this.file.externalRootDirectory;
            }
            else {
                _this.isAndroid = false;
                _this.folderpath = _this.file.documentsDirectory;
            }
        });
    }
    QrPopoverComponent.prototype.ngOnInit = function () {
        setTimeout(function () {
            var qrMainContainer = document.getElementsByTagName('ngx-qrcode')[0];
            if (!qrMainContainer) {
                return console.error('No hay contenedor para la imagen QR');
            }
            var qrImgContainer = qrMainContainer.firstElementChild;
            if (!qrImgContainer) {
                return console.error('No hay imagen QR');
            }
            var child = qrImgContainer.firstElementChild;
            if (child) {
                var src = child.getAttribute('src');
                if (src) {
                    var a = document.getElementById('downladableQr'); // Create <a>
                    a.setAttribute('src', src);
                }
                else {
                    console.error('--- No img src found');
                }
            }
            else {
                console.error('--- Element not found');
            }
        }, 900);
    };
    QrPopoverComponent.prototype.close = function () {
        this.popoverCtrl.dismiss();
    };
    QrPopoverComponent.prototype.saveImage = function (src) {
        return __awaiter(this, void 0, void 0, function () {
            var option, actionSheet, loader, scaledImg, block, dataType, realData, filename;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            header: this.translations.GENERAL.ACTION.options,
                            buttons: [{
                                    text: '500 x 500',
                                    handler: function () { option = 500; }
                                },
                                {
                                    text: '700 x 700',
                                    handler: function () { option = 700; }
                                },
                                {
                                    text: '1200 x 1200',
                                    handler: function () { option = 1200; }
                                }]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        actionSheet.present();
                        return [4 /*yield*/, actionSheet.onDidDismiss()];
                    case 2:
                        _a.sent();
                        if (!option) {
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this.loaderCtrl.create()];
                    case 3:
                        loader = _a.sent();
                        loader.present();
                        return [4 /*yield*/, this.scaleImage(src.getAttribute('src'), option, option)
                                .catch(function (error) {
                                _this.alertCtrl.create({
                                    header: _this.translations.GENERAL.error,
                                    message: _this.translations.QR.scalingError + '<br>' + JSON.stringify(error),
                                    buttons: [{ text: _this.translations.GENERAL.ACTION.ok }]
                                });
                                loader.dismiss();
                            })];
                    case 4:
                        scaledImg = _a.sent();
                        if (!scaledImg) {
                            return [2 /*return*/];
                        }
                        block = (scaledImg || '').split(';');
                        dataType = block[0].split(':')[1];
                        realData = block[1].split(',')[1];
                        filename = "MY_WEBSITE_" + option + "x" + option + "." + dataType.split('/')[1];
                        this.saveBase64(this.folderpath, realData, filename, dataType).then(function (path) { return __awaiter(_this, void 0, void 0, function () {
                            var _a, _b, _c;
                            return __generator(this, function (_d) {
                                switch (_d.label) {
                                    case 0:
                                        this.close();
                                        _b = (_a = this.alertCtrl).create;
                                        _c = {
                                            header: this.translations.QR.completed
                                        };
                                        return [4 /*yield*/, this.translate.get('QR.fileDownloadedOk', { filename: filename }).toPromise()];
                                    case 1:
                                        _b.apply(_a, [(_c.message = _d.sent(),
                                                _c.buttons = [{ text: this.translations.GENERAL.ACTION.ok }],
                                                _c)]).then(function (a) { return a.present(); });
                                        return [2 /*return*/];
                                }
                            });
                        }); }, function (error) {
                            console.error('saveBase64: error', error);
                            _this.alertCtrl.create({
                                header: _this.translations.GENERAL.error,
                                message: _this.translations.QR.fileDownloadError + '<br>' + JSON.stringify(error),
                                buttons: [{ text: _this.translations.GENERAL.ACTION.ok }]
                            }).then(function (a) { return a.present(); });
                        }).finally(function () { return _this.loaderCtrl.dismiss(); });
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Convert a base64 string in a Blob according to the data and contentType.
     *
     * @param b64Data Pure base64 string without contentType
     * @param contentType the content type of the file i.e (image/jpeg - image/png - text/plain)
     * @param sliceSize SliceSize to process the byteCharacters
     * @see http://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
     * @return Blob
     */
    QrPopoverComponent.prototype.b64toBlob = function (b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    };
    QrPopoverComponent.prototype.saveBase64 = function (pictureDir, content, name, dataType) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var blob = _this.b64toBlob(content, dataType);
            if (!_this.isCordovaOrCapacitor) {
                _this.downloadFile(blob, name, dataType);
                return resolve(pictureDir + name);
            }
            _this.file.writeFile(pictureDir, name, blob, { replace: true })
                .then(function () { return resolve(pictureDir + name); })
                .catch(function (err) {
                console.error('error writing blob. ERROR: ', err);
                reject(err);
            });
        });
    };
    QrPopoverComponent.prototype.scaleImage = function (base64Data, width, height) {
        return new Promise(function (resolve, reject) {
            var img = new Image();
            img.onload = function () {
                // We create a canvas and get its context.
                var canvas = document.createElement('canvas');
                var ctx = canvas.getContext('2d');
                // We set the dimensions at the wanted size.
                canvas.width = width;
                canvas.height = height;
                // We resize the image with the canvas method drawImage();
                ctx.drawImage(img, 0, 0, width, height);
                resolve(canvas.toDataURL());
            };
            img.onerror = (function (e) {
                reject(e.toString());
            });
            img.src = base64Data;
        });
    };
    // source: https://davidwalsh.name/javascript-download
    QrPopoverComponent.prototype.downloadFile = function (data, fileName, type) {
        if (type === void 0) { type = 'text/plain'; }
        // Create an invisible A element
        var a = document.createElement('a');
        a.style.display = 'none';
        document.body.appendChild(a);
        // Set the HREF to a Blob representation of the data to be downloaded
        a.href = window.URL.createObjectURL(new Blob([data], { type: type }));
        // Use download attribute to set set desired file name
        a.setAttribute('download', fileName);
        // Trigger the download by simulating click
        a.click();
        // Cleanup
        window.URL.revokeObjectURL(a.href);
        document.body.removeChild(a);
    };
    QrPopoverComponent.ctorParameters = function () { return [
        { type: Platform },
        { type: PopoverController },
        { type: File },
        { type: LoadingController },
        { type: AlertController },
        { type: ActionSheetController },
        { type: TranslateService }
    ]; };
    __decorate([
        Input()
    ], QrPopoverComponent.prototype, "websiteUrlString", void 0);
    QrPopoverComponent = __decorate([
        Component({
            selector: 'boxx-qr-popover',
            template: "<ion-content class=\"ion-padding-bottom\">\n    <div class=\"qr-toolbar\">\n        <ion-button fill=\"clear\" (click)=\"close()\">\n            <ion-icon name=\"close-circle-outline\"></ion-icon>\n        </ion-button>\n    </div>\n    <div class=\"ion-text-center\" *ngIf=\"websiteUrlString\">\n        <ngx-qrcode [elementType]=\"elementType\" [value]=\"websiteUrlString\" cssClass=\"qr-container\"\n            errorCorrectionLevel=\"L\">\n        </ngx-qrcode>\n\n        <ion-button color=\"primary\" id=\"downladableQr\" (click)=\"saveImage($event.target)\" translate>\n            GENERAL.ACTION.download\n        </ion-button>\n\n    </div>\n</ion-content>",
            styles: [":host ion-content{--background:white}:host .qr-toolbar{text-align:right}"]
        })
    ], QrPopoverComponent);
    return QrPopoverComponent;
}());

var QrComponent = /** @class */ (function () {
    function QrComponent(popoverController) {
        this.popoverController = popoverController;
        this.destroyed$ = new Subject();
    }
    QrComponent.prototype.ngOnInit = function () {
    };
    QrComponent.prototype.ngOnDestroy = function () {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    };
    QrComponent.prototype.openQRPopover = function () {
        return __awaiter(this, void 0, void 0, function () {
            var popover;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.popoverController.create({
                            component: QrPopoverComponent,
                            componentProps: {
                                websiteUrlString: this.websiteUrl
                            }
                        })];
                    case 1:
                        popover = _a.sent();
                        return [4 /*yield*/, popover.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    QrComponent.ctorParameters = function () { return [
        { type: PopoverController }
    ]; };
    __decorate([
        Input()
    ], QrComponent.prototype, "isLoading$", void 0);
    __decorate([
        Input()
    ], QrComponent.prototype, "websiteUrl", void 0);
    QrComponent = __decorate([
        Component({
            selector: 'boxx-qr-component',
            template: "<div id=\"qr-spinner-container\" *ngIf=\"(isLoading$ | async) === true\">\n    <ion-spinner  id=\"qr-spinner\"></ion-spinner>\n</div>\n<div *ngIf=\"websiteUrl && (isLoading$ | async) === false\">\n    <ion-icon src=\"assets/icon/qr.svg\" slot=\"end\" (click)=\"openQRPopover()\">\n    </ion-icon>\n</div>\n",
            styles: ["#qr-spinner-container{width:100%;text-align:center}#qr-spinner-container ion-spinner{color:var(--ion-color-primary-contrast,#fff)}ion-icon{border-radius:5px;padding:5px;background:var(--ion-color-primary-contrast,#fff)}"]
        })
    ], QrComponent);
    return QrComponent;
}());

var PANEL_REPOSITORY = new InjectionToken('panelRepository');

var PanelRepository = /** @class */ (function () {
    function PanelRepository(appSettings, httpClient) {
        this.appSettings = appSettings;
        this.httpClient = httpClient;
    }
    PanelRepository.prototype.getPanelData = function () {
        // console.log("--- EXECUTING PanelRepository.getPanelData()");
        return this.httpClient.get("" + this.getBaseUrl());
    };
    PanelRepository.prototype.getBaseUrl = function () {
        return this.appSettings.baseUrl() + "/api/" + this.appSettings.instance() + "/v1/home";
    };
    PanelRepository.ctorParameters = function () { return [
        { type: AbstractAppConfigService, decorators: [{ type: Inject, args: [APP_CONFIG_SERVICE,] }] },
        { type: HttpClient }
    ]; };
    PanelRepository = __decorate([
        Injectable(),
        __param(0, Inject(APP_CONFIG_SERVICE))
    ], PanelRepository);
    return PanelRepository;
}());

var PANEL_SERVICE = new InjectionToken('panelService');

var PanelPageModel = /** @class */ (function () {
    function PanelPageModel(data) {
        this.websiteData = data.websiteData;
        this.contactsData = data.contactsData;
        this.messagesData = data.messagesData;
        this.eventsData = data.eventsData;
    }
    PanelPageModel.fromApiResponse = function (data) {
        var lastContacts = data.contacts.recent_contacts.map(function (c) { return ContactModel.fromDataResponse(c); });
        var lastMessages = data.form_messages.last_received.map(function (m) { return LeadModel.fromApiResponse(m); });
        var upcomingEvents = data.schedule.upcoming_events.map(function (e) { return EventModel.fromDataResponse(e); })
            .sort(function (a, b) { return a.datetimeFrom.localeCompare(b.datetimeFrom); });
        return new PanelPageModel({
            websiteData: { pageViews: +data.analytics.page_views },
            contactsData: {
                lastContacts: lastContacts,
                stats: {
                    total: +data.contacts.stats.total,
                    client: +data.contacts.stats.clients,
                    prospect: +data.contacts.stats.prospects,
                    notSpecified: +data.contacts.stats.not_specified
                }
            },
            messagesData: {
                lastMessages: lastMessages,
                stats: {
                    total: +data.form_messages.stats.total,
                    read: +data.form_messages.stats.read,
                    unread: +data.form_messages.stats.unread,
                }
            },
            eventsData: {
                upcomingEvents: upcomingEvents,
                stats: { total: +data.schedule.stats.total }
            }
        });
    };
    PanelPageModel.empty = function () {
        return new PanelPageModel({
            websiteData: { pageViews: 0 },
            contactsData: {
                lastContacts: [],
                stats: {
                    total: 0,
                    client: 0,
                    prospect: 0,
                    notSpecified: 0
                }
            },
            messagesData: {
                lastMessages: [],
                stats: {
                    total: 0,
                    read: 0,
                    unread: 0,
                }
            },
            eventsData: {
                upcomingEvents: [],
                stats: { total: 0 }
            }
        });
    };
    return PanelPageModel;
}());

var PanelService = /** @class */ (function () {
    function PanelService(repository) {
        this.repository = repository;
    }
    PanelService.prototype.getPanelData = function () {
        return this.repository.getPanelData().pipe(map(function (response) {
            return PanelPageModel.fromApiResponse(response.data);
        }), catchError(function (error) {
            throw error;
        }));
    };
    PanelService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [PANEL_REPOSITORY,] }] }
    ]; };
    PanelService.ɵprov = ɵɵdefineInjectable({ factory: function PanelService_Factory() { return new PanelService(ɵɵinject(PANEL_REPOSITORY)); }, token: PanelService, providedIn: "root" });
    PanelService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __param(0, Inject(PANEL_REPOSITORY))
    ], PanelService);
    return PanelService;
}());

var PanelActionTypes;
(function (PanelActionTypes) {
    PanelActionTypes["LoadPanelBegin"] = "[PANEL] Load Panel Begin";
    PanelActionTypes["LoadPanelSuccess"] = "[PANEL] Load Panel Success";
    PanelActionTypes["LoadPanelFail"] = "[PANEL] Load Panel Fail";
})(PanelActionTypes || (PanelActionTypes = {}));
var LoadPanelBeginAction = createAction(PanelActionTypes.LoadPanelBegin);
var LoadPanelSuccessAction = createAction(PanelActionTypes.LoadPanelSuccess, props());
var LoadPanelFailAction = createAction(PanelActionTypes.LoadPanelFail, props());

var PanelEffects = /** @class */ (function () {
    function PanelEffects(actions$, service) {
        var _this = this;
        this.actions$ = actions$;
        this.service = service;
        this.loadPanel$ = createEffect(function () { return _this.actions$.pipe(ofType(PanelActionTypes.LoadPanelBegin), switchMap(function () {
            return _this.service.getPanelData().pipe(map(function (panel) { return LoadPanelSuccessAction({ payload: panel }); }), catchError(function (error) {
                console.error('Couldn\'t load main panel', error);
                return of(LoadPanelFailAction({ errors: error }));
            }));
        })); });
    }
    PanelEffects.ctorParameters = function () { return [
        { type: Actions },
        { type: undefined, decorators: [{ type: Inject, args: [PANEL_SERVICE,] }] }
    ]; };
    PanelEffects = __decorate([
        Injectable(),
        __param(1, Inject(PANEL_SERVICE))
    ], PanelEffects);
    return PanelEffects;
}());

var ɵ0 = PanelPageModel.empty();
var initialState = {
    isLoading: false,
    hasBeenFetched: false,
    data: ɵ0,
    error: null,
    success: null
};
var ɵ1 = function (state) { return (__assign(__assign({}, state), { error: null, success: null, isLoading: true })); }, ɵ2 = function (state, action) { return (__assign(__assign({}, state), { hasBeenFetched: true, data: action.payload, isLoading: false, error: null, success: { after: getSuccessActionType(action.type) } })); }, ɵ3 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, error: { after: getErrorActionType(action.type), error: action.errors } })); };
var reducer = createReducer(initialState, on(LoadPanelBeginAction, ɵ1), on(LoadPanelSuccessAction, ɵ2), on(LoadPanelFailAction, ɵ3));
function getErrorActionType(type) {
    var action = 'UNKNOWN';
    switch (type) {
        case PanelActionTypes.LoadPanelFail:
            action = 'GET';
            break;
    }
    return action;
}
function getSuccessActionType(type) {
    var action = 'UNKNOWN';
    switch (type) {
        case PanelActionTypes.LoadPanelSuccess:
            action = 'GET';
            break;
    }
    return action;
}
function panelReducer(state, action) {
    return reducer(state, action);
}

var getPanelState = createFeatureSelector('panel');
var ɵ0$1 = function (state) { return state; };
var getPanelPageState = createSelector(getPanelState, ɵ0$1);
var stateGetIsLoading = function (state) { return state.isLoading; };
var ɵ1$1 = stateGetIsLoading;
var stateGetData = function (state) { return state.data; };
var getIsLoading = createSelector(getPanelPageState, stateGetIsLoading);
var ɵ2$1 = function (state) { return state.error; };
var getError = createSelector(getPanelPageState, ɵ2$1);
var ɵ3$1 = function (state) { return state.success; };
var getSuccess = createSelector(getPanelPageState, ɵ3$1);
var getData = createSelector(getPanelPageState, stateGetData);
var ɵ4 = function (state) { return state.hasBeenFetched; };
var hasBeenFetched = createSelector(getPanelPageState, ɵ4);

var PanelStore = /** @class */ (function () {
    function PanelStore(store) {
        this.store = store;
    }
    Object.defineProperty(PanelStore.prototype, "Loading$", {
        get: function () { return this.store.select(getIsLoading); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PanelStore.prototype, "Error$", {
        get: function () { return this.store.select(getError); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PanelStore.prototype, "Success$", {
        get: function () { return this.store.select(getSuccess); },
        enumerable: true,
        configurable: true
    });
    PanelStore.prototype.loadPanel = function () {
        return this.store.dispatch(LoadPanelBeginAction());
    };
    Object.defineProperty(PanelStore.prototype, "Panel$", {
        get: function () {
            return this.store.select(getData);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PanelStore.prototype, "HasBeenFetched$", {
        get: function () {
            return this.store.select(hasBeenFetched);
        },
        enumerable: true,
        configurable: true
    });
    PanelStore.ctorParameters = function () { return [
        { type: Store }
    ]; };
    PanelStore = __decorate([
        Injectable()
    ], PanelStore);
    return PanelStore;
}());

var PanelCoreModule = /** @class */ (function () {
    function PanelCoreModule() {
    }
    PanelCoreModule_1 = PanelCoreModule;
    PanelCoreModule.forRoot = function (config) {
        return {
            ngModule: PanelCoreModule_1,
            providers: __spread([
                { provide: PANEL_SERVICE, useClass: PanelService },
                { provide: PANEL_REPOSITORY, useClass: PanelRepository }
            ], config.providers, [
                PanelStore
            ])
        };
    };
    var PanelCoreModule_1;
    PanelCoreModule = PanelCoreModule_1 = __decorate([
        NgModule({
            declarations: [
                ContactChartWidgetComponent,
                KpiCellComponent,
                MainHeaderComponent,
                PanelWidgetComponent,
                QrComponent,
                QrPopoverComponent,
            ],
            imports: [
                HttpClientModule,
                StoreModule.forFeature('panel', panelReducer),
                EffectsModule.forFeature([PanelEffects]),
                TranslateModule.forChild(),
                CommonModule,
                FormsModule,
                IonicModule,
                NgxQRCodeModule
            ],
            exports: [
                ContactChartWidgetComponent,
                KpiCellComponent,
                MainHeaderComponent,
                PanelWidgetComponent,
                QrComponent,
                QrPopoverComponent,
            ]
        })
    ], PanelCoreModule);
    return PanelCoreModule;
}());

/*
 * Public API Surface of panel-core
 */

/**
 * Generated bundle index. Do not edit.
 */

export { LoadPanelBeginAction, LoadPanelFailAction, LoadPanelSuccessAction, PANEL_REPOSITORY, PANEL_SERVICE, PanelActionTypes, PanelCoreModule, PanelEffects, PanelPageModel, PanelRepository, PanelService, PanelStore, getData, getError, getIsLoading, getPanelPageState, getPanelState, getSuccess, hasBeenFetched, initialState, panelReducer, stateGetData, ɵ0$1 as ɵ0, ɵ1$1 as ɵ1, ɵ2$1 as ɵ2, ɵ3$1 as ɵ3, ɵ4, ContactChartWidgetComponent as ɵa, KpiCellComponent as ɵb, MainHeaderComponent as ɵc, PanelWidgetComponent as ɵd, QrComponent as ɵe, QrPopoverComponent as ɵf };
//# sourceMappingURL=boxx-panel-core.js.map
