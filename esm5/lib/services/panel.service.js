import { __decorate, __param } from "tslib";
import { Inject, Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { PanelPageModel } from '../models/panel.model';
import { PANEL_REPOSITORY } from '../repositories/IPanel.repository';
import * as i0 from "@angular/core";
import * as i1 from "../repositories/IPanel.repository";
var PanelService = /** @class */ (function () {
    function PanelService(repository) {
        this.repository = repository;
    }
    PanelService.prototype.getPanelData = function () {
        return this.repository.getPanelData().pipe(map(function (response) {
            return PanelPageModel.fromApiResponse(response.data);
        }), catchError(function (error) {
            throw error;
        }));
    };
    PanelService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [PANEL_REPOSITORY,] }] }
    ]; };
    PanelService.ɵprov = i0.ɵɵdefineInjectable({ factory: function PanelService_Factory() { return new PanelService(i0.ɵɵinject(i1.PANEL_REPOSITORY)); }, token: PanelService, providedIn: "root" });
    PanelService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __param(0, Inject(PANEL_REPOSITORY))
    ], PanelService);
    return PanelService;
}());
export { PanelService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGFuZWwuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3BhbmVsLWNvcmUvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvUGFuZWwuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHbkQsT0FBTyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDdkQsT0FBTyxFQUF1QyxnQkFBZ0IsRUFBRSxNQUFNLG1DQUFtQyxDQUFDOzs7QUFPMUc7SUFDSSxzQkFDc0MsVUFBNEI7UUFBNUIsZUFBVSxHQUFWLFVBQVUsQ0FBa0I7SUFDOUQsQ0FBQztJQUVMLG1DQUFZLEdBQVo7UUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLENBQUMsSUFBSSxDQUN0QyxHQUFHLENBQUMsVUFBQyxRQUErQztZQUVoRCxPQUFPLGNBQWMsQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pELENBQUMsQ0FBQyxFQUNGLFVBQVUsQ0FBQyxVQUFBLEtBQUs7WUFDWixNQUFNLEtBQUssQ0FBQztRQUNoQixDQUFDLENBQUMsQ0FDTCxDQUFDO0lBQ04sQ0FBQzs7Z0RBYkksTUFBTSxTQUFDLGdCQUFnQjs7O0lBRm5CLFlBQVk7UUFIeEIsVUFBVSxDQUFDO1lBQ1IsVUFBVSxFQUFFLE1BQU07U0FDckIsQ0FBQztRQUdPLFdBQUEsTUFBTSxDQUFDLGdCQUFnQixDQUFDLENBQUE7T0FGcEIsWUFBWSxDQWdCeEI7dUJBNUJEO0NBNEJDLEFBaEJELElBZ0JDO1NBaEJZLFlBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IElIdHRwQmFzaWNSZXNwb25zZSB9IGZyb20gJ0Bib3h4L2NvcmUnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgY2F0Y2hFcnJvciwgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgUGFuZWxQYWdlTW9kZWwgfSBmcm9tICcuLi9tb2RlbHMvcGFuZWwubW9kZWwnO1xuaW1wb3J0IHsgSVBhbmVsQXBpUmVzcG9uc2UsIElQYW5lbFJlcG9zaXRvcnksIFBBTkVMX1JFUE9TSVRPUlkgfSBmcm9tICcuLi9yZXBvc2l0b3JpZXMvSVBhbmVsLnJlcG9zaXRvcnknO1xuaW1wb3J0IHsgSVBhbmVsU2VydmljZSB9IGZyb20gJy4vSVBhbmVsLnNlcnZpY2UnO1xuXG5cbkBJbmplY3RhYmxlKHtcbiAgICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgUGFuZWxTZXJ2aWNlIGltcGxlbWVudHMgSVBhbmVsU2VydmljZSB7XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIEBJbmplY3QoUEFORUxfUkVQT1NJVE9SWSkgcHJpdmF0ZSByZXBvc2l0b3J5OiBJUGFuZWxSZXBvc2l0b3J5XG4gICAgKSB7IH1cblxuICAgIGdldFBhbmVsRGF0YSgpOiBPYnNlcnZhYmxlPFBhbmVsUGFnZU1vZGVsPiB7XG4gICAgICAgIHJldHVybiB0aGlzLnJlcG9zaXRvcnkuZ2V0UGFuZWxEYXRhKCkucGlwZShcbiAgICAgICAgICAgIG1hcCgocmVzcG9uc2U6IElIdHRwQmFzaWNSZXNwb25zZTxJUGFuZWxBcGlSZXNwb25zZT4pID0+IHtcblxuICAgICAgICAgICAgICAgIHJldHVybiBQYW5lbFBhZ2VNb2RlbC5mcm9tQXBpUmVzcG9uc2UocmVzcG9uc2UuZGF0YSk7XG4gICAgICAgICAgICB9KSxcbiAgICAgICAgICAgIGNhdGNoRXJyb3IoZXJyb3IgPT4ge1xuICAgICAgICAgICAgICAgIHRocm93IGVycm9yO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgKTtcbiAgICB9XG59XG4iXX0=