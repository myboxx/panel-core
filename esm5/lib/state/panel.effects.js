import { __decorate, __param } from "tslib";
import { Injectable, Inject } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { switchMap, catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { PANEL_SERVICE } from '../services/IPanel.service';
import * as PanelActions from './panel.actions';
var PanelEffects = /** @class */ (function () {
    function PanelEffects(actions$, service) {
        var _this = this;
        this.actions$ = actions$;
        this.service = service;
        this.loadPanel$ = createEffect(function () { return _this.actions$.pipe(ofType(PanelActions.PanelActionTypes.LoadPanelBegin), switchMap(function () {
            return _this.service.getPanelData().pipe(map(function (panel) { return PanelActions.LoadPanelSuccessAction({ payload: panel }); }), catchError(function (error) {
                console.error('Couldn\'t load main panel', error);
                return of(PanelActions.LoadPanelFailAction({ errors: error }));
            }));
        })); });
    }
    PanelEffects.ctorParameters = function () { return [
        { type: Actions },
        { type: undefined, decorators: [{ type: Inject, args: [PANEL_SERVICE,] }] }
    ]; };
    PanelEffects = __decorate([
        Injectable(),
        __param(1, Inject(PANEL_SERVICE))
    ], PanelEffects);
    return PanelEffects;
}());
export { PanelEffects };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWwuZWZmZWN0cy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3BhbmVsLWNvcmUvIiwic291cmNlcyI6WyJsaWIvc3RhdGUvcGFuZWwuZWZmZWN0cy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbkQsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzVELE9BQU8sRUFBRSxFQUFFLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDMUIsT0FBTyxFQUFpQixhQUFhLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUMxRSxPQUFPLEtBQUssWUFBWSxNQUFNLGlCQUFpQixDQUFDO0FBR2hEO0lBZ0JJLHNCQUNZLFFBQWlCLEVBQ00sT0FBc0I7UUFGekQsaUJBR0s7UUFGTyxhQUFRLEdBQVIsUUFBUSxDQUFTO1FBQ00sWUFBTyxHQUFQLE9BQU8sQ0FBZTtRQWpCekQsZUFBVSxHQUFHLFlBQVksQ0FDckIsY0FBTSxPQUFBLEtBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUNwQixNQUFNLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxFQUNwRCxTQUFTLENBQUM7WUFDTixPQUFPLEtBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLENBQUMsSUFBSSxDQUNuQyxHQUFHLENBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxZQUFZLENBQUMsc0JBQXNCLENBQUMsRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBdkQsQ0FBdUQsQ0FBQyxFQUNyRSxVQUFVLENBQUMsVUFBQSxLQUFLO2dCQUNaLE9BQU8sQ0FBQyxLQUFLLENBQUMsMkJBQTJCLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ2xELE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDbkUsQ0FBQyxDQUFDLENBQ0wsQ0FBQztRQUNOLENBQUMsQ0FBQyxDQUNMLEVBWEssQ0FXTCxDQUNKLENBQUM7SUFLRSxDQUFDOztnQkFGaUIsT0FBTztnREFDeEIsTUFBTSxTQUFDLGFBQWE7O0lBbEJoQixZQUFZO1FBRHhCLFVBQVUsRUFBRTtRQW1CSixXQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQTtPQWxCakIsWUFBWSxDQW9CeEI7SUFBRCxtQkFBQztDQUFBLEFBcEJELElBb0JDO1NBcEJZLFlBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEFjdGlvbnMsIG9mVHlwZSwgY3JlYXRlRWZmZWN0IH0gZnJvbSAnQG5ncngvZWZmZWN0cyc7XG5pbXBvcnQgeyBzd2l0Y2hNYXAsIGNhdGNoRXJyb3IsIG1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IG9mIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBJUGFuZWxTZXJ2aWNlLCBQQU5FTF9TRVJWSUNFIH0gZnJvbSAnLi4vc2VydmljZXMvSVBhbmVsLnNlcnZpY2UnO1xuaW1wb3J0ICogYXMgUGFuZWxBY3Rpb25zIGZyb20gJy4vcGFuZWwuYWN0aW9ucyc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBQYW5lbEVmZmVjdHMge1xuICAgIGxvYWRQYW5lbCQgPSBjcmVhdGVFZmZlY3QoXG4gICAgICAgICgpID0+IHRoaXMuYWN0aW9ucyQucGlwZShcbiAgICAgICAgICAgIG9mVHlwZShQYW5lbEFjdGlvbnMuUGFuZWxBY3Rpb25UeXBlcy5Mb2FkUGFuZWxCZWdpbiksXG4gICAgICAgICAgICBzd2l0Y2hNYXAoKCkgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnNlcnZpY2UuZ2V0UGFuZWxEYXRhKCkucGlwZShcbiAgICAgICAgICAgICAgICAgICAgbWFwKHBhbmVsID0+IFBhbmVsQWN0aW9ucy5Mb2FkUGFuZWxTdWNjZXNzQWN0aW9uKHsgcGF5bG9hZDogcGFuZWwgfSkpLFxuICAgICAgICAgICAgICAgICAgICBjYXRjaEVycm9yKGVycm9yID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0NvdWxkblxcJ3QgbG9hZCBtYWluIHBhbmVsJywgZXJyb3IpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG9mKFBhbmVsQWN0aW9ucy5Mb2FkUGFuZWxGYWlsQWN0aW9uKHsgZXJyb3JzOiBlcnJvciB9KSk7XG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgIClcbiAgICApO1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByaXZhdGUgYWN0aW9ucyQ6IEFjdGlvbnMsXG4gICAgICAgIEBJbmplY3QoUEFORUxfU0VSVklDRSkgcHJpdmF0ZSBzZXJ2aWNlOiBJUGFuZWxTZXJ2aWNlXG4gICAgKSB7IH1cbn1cbiJdfQ==