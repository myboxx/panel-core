import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromSelector from './panel.selectors';
import * as fromActions from './panel.actions';
var PanelStore = /** @class */ (function () {
    function PanelStore(store) {
        this.store = store;
    }
    Object.defineProperty(PanelStore.prototype, "Loading$", {
        get: function () { return this.store.select(fromSelector.getIsLoading); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PanelStore.prototype, "Error$", {
        get: function () { return this.store.select(fromSelector.getError); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PanelStore.prototype, "Success$", {
        get: function () { return this.store.select(fromSelector.getSuccess); },
        enumerable: true,
        configurable: true
    });
    PanelStore.prototype.loadPanel = function () {
        return this.store.dispatch(fromActions.LoadPanelBeginAction());
    };
    Object.defineProperty(PanelStore.prototype, "Panel$", {
        get: function () {
            return this.store.select(fromSelector.getData);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PanelStore.prototype, "HasBeenFetched$", {
        get: function () {
            return this.store.select(fromSelector.hasBeenFetched);
        },
        enumerable: true,
        configurable: true
    });
    PanelStore.ctorParameters = function () { return [
        { type: Store }
    ]; };
    PanelStore = __decorate([
        Injectable()
    ], PanelStore);
    return PanelStore;
}());
export { PanelStore };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWwuc3RvcmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9wYW5lbC1jb3JlLyIsInNvdXJjZXMiOlsibGliL3N0YXRlL3BhbmVsLnN0b3JlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDcEMsT0FBTyxLQUFLLFlBQVksTUFBTSxtQkFBbUIsQ0FBQztBQUNsRCxPQUFPLEtBQUssV0FBVyxNQUFNLGlCQUFpQixDQUFDO0FBSS9DO0lBQ0ksb0JBQW9CLEtBQW9DO1FBQXBDLFVBQUssR0FBTCxLQUFLLENBQStCO0lBQUksQ0FBQztJQUU3RCxzQkFBSSxnQ0FBUTthQUFaLGNBQWlCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQzs7O09BQUE7SUFFdkUsc0JBQUksOEJBQU07YUFBVixjQUFlLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7O09BQUE7SUFFakUsc0JBQUksZ0NBQVE7YUFBWixjQUFpQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7OztPQUFBO0lBRXJFLDhCQUFTLEdBQVQ7UUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLENBQUM7SUFDbkUsQ0FBQztJQUVELHNCQUFJLDhCQUFNO2FBQVY7WUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNuRCxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLHVDQUFlO2FBQW5CO1lBQ0ksT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDMUQsQ0FBQzs7O09BQUE7O2dCQWxCMEIsS0FBSzs7SUFEdkIsVUFBVTtRQUR0QixVQUFVLEVBQUU7T0FDQSxVQUFVLENBb0J0QjtJQUFELGlCQUFDO0NBQUEsQUFwQkQsSUFvQkM7U0FwQlksVUFBVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFN0b3JlIH0gZnJvbSAnQG5ncngvc3RvcmUnO1xuaW1wb3J0ICogYXMgZnJvbVNlbGVjdG9yIGZyb20gJy4vcGFuZWwuc2VsZWN0b3JzJztcbmltcG9ydCAqIGFzIGZyb21BY3Rpb25zIGZyb20gJy4vcGFuZWwuYWN0aW9ucyc7XG5pbXBvcnQgKiBhcyBmcm9tUmVkdWNlciBmcm9tICcuL3BhbmVsLnJlZHVjZXInO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgUGFuZWxTdG9yZSB7XG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBzdG9yZTogU3RvcmU8ZnJvbVJlZHVjZXIuUGFuZWxTdGF0ZT4pIHsgfVxuXG4gICAgZ2V0IExvYWRpbmckKCkgeyByZXR1cm4gdGhpcy5zdG9yZS5zZWxlY3QoZnJvbVNlbGVjdG9yLmdldElzTG9hZGluZyk7IH1cblxuICAgIGdldCBFcnJvciQoKSB7IHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3IuZ2V0RXJyb3IpOyB9XG5cbiAgICBnZXQgU3VjY2VzcyQoKSB7IHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3IuZ2V0U3VjY2Vzcyk7IH1cblxuICAgIGxvYWRQYW5lbCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RvcmUuZGlzcGF0Y2goZnJvbUFjdGlvbnMuTG9hZFBhbmVsQmVnaW5BY3Rpb24oKSk7XG4gICAgfVxuXG4gICAgZ2V0IFBhbmVsJCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3Rvci5nZXREYXRhKTtcbiAgICB9XG5cbiAgICBnZXQgSGFzQmVlbkZldGNoZWQkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zdG9yZS5zZWxlY3QoZnJvbVNlbGVjdG9yLmhhc0JlZW5GZXRjaGVkKTtcbiAgICB9XG59XG4iXX0=