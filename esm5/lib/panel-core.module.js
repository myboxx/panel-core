import { __decorate, __read, __spread } from "tslib";
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { TranslateModule } from '@ngx-translate/core';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { ContactChartWidgetComponent } from './components/contact-chart-widget/contact-chart-widget.component';
import { KpiCellComponent } from './components/kpi-cell/kpi-cell.component';
import { MainHeaderComponent } from './components/main-heder/main-header.component';
import { PanelWidgetComponent } from './components/panel-widget/panel-widget.component';
import { QrComponent } from './components/qr-component/qr-component';
import { QrPopoverComponent } from './components/qr-component/qr-popover.component';
import { PANEL_REPOSITORY } from './repositories/IPanel.repository';
import { PanelRepository } from './repositories/panel.repository';
import { PANEL_SERVICE } from './services/IPanel.service';
import { PanelService } from './services/Panel.service';
import { PanelEffects } from './state/panel.effects';
import { panelReducer } from './state/panel.reducer';
import { PanelStore } from './state/panel.store';
var PanelCoreModule = /** @class */ (function () {
    function PanelCoreModule() {
    }
    PanelCoreModule_1 = PanelCoreModule;
    PanelCoreModule.forRoot = function (config) {
        return {
            ngModule: PanelCoreModule_1,
            providers: __spread([
                { provide: PANEL_SERVICE, useClass: PanelService },
                { provide: PANEL_REPOSITORY, useClass: PanelRepository }
            ], config.providers, [
                PanelStore
            ])
        };
    };
    var PanelCoreModule_1;
    PanelCoreModule = PanelCoreModule_1 = __decorate([
        NgModule({
            declarations: [
                ContactChartWidgetComponent,
                KpiCellComponent,
                MainHeaderComponent,
                PanelWidgetComponent,
                QrComponent,
                QrPopoverComponent,
            ],
            imports: [
                HttpClientModule,
                StoreModule.forFeature('panel', panelReducer),
                EffectsModule.forFeature([PanelEffects]),
                TranslateModule.forChild(),
                CommonModule,
                FormsModule,
                IonicModule,
                NgxQRCodeModule
            ],
            exports: [
                ContactChartWidgetComponent,
                KpiCellComponent,
                MainHeaderComponent,
                PanelWidgetComponent,
                QrComponent,
                QrPopoverComponent,
            ]
        })
    ], PanelCoreModule);
    return PanelCoreModule;
}());
export { PanelCoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWwtY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9wYW5lbC1jb3JlLyIsInNvdXJjZXMiOlsibGliL3BhbmVsLWNvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDeEQsT0FBTyxFQUF1QixRQUFRLEVBQVksTUFBTSxlQUFlLENBQUM7QUFDeEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzlDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDMUMsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3RELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUM1RCxPQUFPLEVBQUUsMkJBQTJCLEVBQUUsTUFBTSxrRUFBa0UsQ0FBQztBQUMvRyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUM1RSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUNwRixPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUN4RixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDckUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDcEYsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDcEUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBQ2xFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDeEQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3JELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUNyRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFrQ2pEO0lBQUE7SUFZQSxDQUFDO3dCQVpZLGVBQWU7SUFDakIsdUJBQU8sR0FBZCxVQUFlLE1BQThCO1FBQ3pDLE9BQU87WUFDSCxRQUFRLEVBQUUsaUJBQWU7WUFDekIsU0FBUztnQkFDTCxFQUFFLE9BQU8sRUFBRSxhQUFhLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRTtnQkFDbEQsRUFBRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsUUFBUSxFQUFFLGVBQWUsRUFBRTtlQUNyRCxNQUFNLENBQUMsU0FBUztnQkFDbkIsVUFBVTtjQUNiO1NBQ0osQ0FBQztJQUNOLENBQUM7O0lBWFEsZUFBZTtRQTVCM0IsUUFBUSxDQUFDO1lBQ1IsWUFBWSxFQUFFO2dCQUNaLDJCQUEyQjtnQkFDM0IsZ0JBQWdCO2dCQUNoQixtQkFBbUI7Z0JBQ25CLG9CQUFvQjtnQkFDcEIsV0FBVztnQkFDWCxrQkFBa0I7YUFDbkI7WUFDRCxPQUFPLEVBQUU7Z0JBQ1AsZ0JBQWdCO2dCQUNoQixXQUFXLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxZQUFZLENBQUM7Z0JBQzdDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDeEMsZUFBZSxDQUFDLFFBQVEsRUFBRTtnQkFDMUIsWUFBWTtnQkFDWixXQUFXO2dCQUNYLFdBQVc7Z0JBQ1gsZUFBZTthQUNoQjtZQUNELE9BQU8sRUFBRTtnQkFDUCwyQkFBMkI7Z0JBQzNCLGdCQUFnQjtnQkFDaEIsbUJBQW1CO2dCQUNuQixvQkFBb0I7Z0JBQ3BCLFdBQVc7Z0JBQ1gsa0JBQWtCO2FBQ25CO1NBQ0YsQ0FBQztPQUNXLGVBQWUsQ0FZM0I7SUFBRCxzQkFBQztDQUFBLEFBWkQsSUFZQztTQVpZLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgSHR0cENsaWVudE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE1vZHVsZVdpdGhQcm92aWRlcnMsIE5nTW9kdWxlLCBQcm92aWRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBJb25pY01vZHVsZSB9IGZyb20gJ0Bpb25pYy9hbmd1bGFyJztcbmltcG9ydCB7IEVmZmVjdHNNb2R1bGUgfSBmcm9tICdAbmdyeC9lZmZlY3RzJztcbmltcG9ydCB7IFN0b3JlTW9kdWxlIH0gZnJvbSAnQG5ncngvc3RvcmUnO1xuaW1wb3J0IHsgVHJhbnNsYXRlTW9kdWxlIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XG5pbXBvcnQgeyBOZ3hRUkNvZGVNb2R1bGUgfSBmcm9tICdAdGVjaGllZGlhcmllcy9uZ3gtcXJjb2RlJztcbmltcG9ydCB7IENvbnRhY3RDaGFydFdpZGdldENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jb250YWN0LWNoYXJ0LXdpZGdldC9jb250YWN0LWNoYXJ0LXdpZGdldC5jb21wb25lbnQnO1xuaW1wb3J0IHsgS3BpQ2VsbENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9rcGktY2VsbC9rcGktY2VsbC5jb21wb25lbnQnO1xuaW1wb3J0IHsgTWFpbkhlYWRlckNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9tYWluLWhlZGVyL21haW4taGVhZGVyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBQYW5lbFdpZGdldENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9wYW5lbC13aWRnZXQvcGFuZWwtd2lkZ2V0LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBRckNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9xci1jb21wb25lbnQvcXItY29tcG9uZW50JztcbmltcG9ydCB7IFFyUG9wb3ZlckNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9xci1jb21wb25lbnQvcXItcG9wb3Zlci5jb21wb25lbnQnO1xuaW1wb3J0IHsgUEFORUxfUkVQT1NJVE9SWSB9IGZyb20gJy4vcmVwb3NpdG9yaWVzL0lQYW5lbC5yZXBvc2l0b3J5JztcbmltcG9ydCB7IFBhbmVsUmVwb3NpdG9yeSB9IGZyb20gJy4vcmVwb3NpdG9yaWVzL3BhbmVsLnJlcG9zaXRvcnknO1xuaW1wb3J0IHsgUEFORUxfU0VSVklDRSB9IGZyb20gJy4vc2VydmljZXMvSVBhbmVsLnNlcnZpY2UnO1xuaW1wb3J0IHsgUGFuZWxTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9QYW5lbC5zZXJ2aWNlJztcbmltcG9ydCB7IFBhbmVsRWZmZWN0cyB9IGZyb20gJy4vc3RhdGUvcGFuZWwuZWZmZWN0cyc7XG5pbXBvcnQgeyBwYW5lbFJlZHVjZXIgfSBmcm9tICcuL3N0YXRlL3BhbmVsLnJlZHVjZXInO1xuaW1wb3J0IHsgUGFuZWxTdG9yZSB9IGZyb20gJy4vc3RhdGUvcGFuZWwuc3RvcmUnO1xuXG5pbnRlcmZhY2UgTW9kdWxlT3B0aW9uc0ludGVyZmFjZSB7XG4gICAgcHJvdmlkZXJzOiBQcm92aWRlcltdO1xufVxuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBDb250YWN0Q2hhcnRXaWRnZXRDb21wb25lbnQsXG4gICAgS3BpQ2VsbENvbXBvbmVudCxcbiAgICBNYWluSGVhZGVyQ29tcG9uZW50LFxuICAgIFBhbmVsV2lkZ2V0Q29tcG9uZW50LFxuICAgIFFyQ29tcG9uZW50LFxuICAgIFFyUG9wb3ZlckNvbXBvbmVudCxcbiAgXSxcbiAgaW1wb3J0czogW1xuICAgIEh0dHBDbGllbnRNb2R1bGUsXG4gICAgU3RvcmVNb2R1bGUuZm9yRmVhdHVyZSgncGFuZWwnLCBwYW5lbFJlZHVjZXIpLFxuICAgIEVmZmVjdHNNb2R1bGUuZm9yRmVhdHVyZShbUGFuZWxFZmZlY3RzXSksXG4gICAgVHJhbnNsYXRlTW9kdWxlLmZvckNoaWxkKCksXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIEZvcm1zTW9kdWxlLFxuICAgIElvbmljTW9kdWxlLFxuICAgIE5neFFSQ29kZU1vZHVsZVxuICBdLFxuICBleHBvcnRzOiBbXG4gICAgQ29udGFjdENoYXJ0V2lkZ2V0Q29tcG9uZW50LFxuICAgIEtwaUNlbGxDb21wb25lbnQsXG4gICAgTWFpbkhlYWRlckNvbXBvbmVudCxcbiAgICBQYW5lbFdpZGdldENvbXBvbmVudCxcbiAgICBRckNvbXBvbmVudCxcbiAgICBRclBvcG92ZXJDb21wb25lbnQsXG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgUGFuZWxDb3JlTW9kdWxlIHtcbiAgICBzdGF0aWMgZm9yUm9vdChjb25maWc6IE1vZHVsZU9wdGlvbnNJbnRlcmZhY2UpOiBNb2R1bGVXaXRoUHJvdmlkZXJzPFBhbmVsQ29yZU1vZHVsZT4ge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgbmdNb2R1bGU6IFBhbmVsQ29yZU1vZHVsZSxcbiAgICAgICAgICAgIHByb3ZpZGVyczogW1xuICAgICAgICAgICAgICAgIHsgcHJvdmlkZTogUEFORUxfU0VSVklDRSwgdXNlQ2xhc3M6IFBhbmVsU2VydmljZSB9LFxuICAgICAgICAgICAgICAgIHsgcHJvdmlkZTogUEFORUxfUkVQT1NJVE9SWSwgdXNlQ2xhc3M6IFBhbmVsUmVwb3NpdG9yeSB9LFxuICAgICAgICAgICAgICAgIC4uLmNvbmZpZy5wcm92aWRlcnMsXG4gICAgICAgICAgICAgICAgUGFuZWxTdG9yZVxuICAgICAgICAgICAgXVxuICAgICAgICB9O1xuICAgIH1cbn1cbiJdfQ==