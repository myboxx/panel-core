import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
var KpiCellComponent = /** @class */ (function () {
    function KpiCellComponent() {
    }
    KpiCellComponent.prototype.ngOnInit = function () { };
    __decorate([
        Input()
    ], KpiCellComponent.prototype, "className", void 0);
    __decorate([
        Input()
    ], KpiCellComponent.prototype, "iconName", void 0);
    __decorate([
        Input()
    ], KpiCellComponent.prototype, "iconSrc", void 0);
    __decorate([
        Input()
    ], KpiCellComponent.prototype, "label", void 0);
    __decorate([
        Input()
    ], KpiCellComponent.prototype, "value", void 0);
    KpiCellComponent = __decorate([
        Component({
            selector: 'boxx-kpi-cell',
            template: "<!-- KPI CELL -->\n<div class=\"kpi {{className}}\">\n    <div class=\"kpi-icon\">\n        <ion-icon name=\"{{iconName}}\" src=\"{{iconSrc}}\" [hidden]=\"!iconName && !iconSrc\"></ion-icon>\n    </div>\n    <div class=\"kpi-label\">\n        {{label}}\n    </div>\n    <div class=\"kpi-counter\">\n        <div class=\"value\">{{value}}</div>\n    </div>\n</div>",
            styles: [":host{display:contents}:host .kpi{text-align:center;min-width:120px;max-width:100%;min-height:125px;max-height:300px;padding:10px;box-shadow:#d3d3d3 0 0 4px;border-radius:29px;background-color:#fff;position:relative;display:-ms-grid;display:grid}:host .kpi ion-icon{font-size:x-large;display:flex;margin:auto}"]
        })
    ], KpiCellComponent);
    return KpiCellComponent;
}());
export { KpiCellComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoia3BpLWNlbGwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvcGFuZWwtY29yZS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2twaS1jZWxsL2twaS1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFPekQ7SUFPRTtJQUFnQixDQUFDO0lBRWpCLG1DQUFRLEdBQVIsY0FBWSxDQUFDO0lBUko7UUFBUixLQUFLLEVBQUU7dURBQW1CO0lBQ2xCO1FBQVIsS0FBSyxFQUFFO3NEQUFrQjtJQUNqQjtRQUFSLEtBQUssRUFBRTtxREFBaUI7SUFDaEI7UUFBUixLQUFLLEVBQUU7bURBQWU7SUFDZDtRQUFSLEtBQUssRUFBRTttREFBZTtJQUxaLGdCQUFnQjtRQUw1QixTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsZUFBZTtZQUN6Qix1WEFBd0M7O1NBRXpDLENBQUM7T0FDVyxnQkFBZ0IsQ0FXNUI7SUFBRCx1QkFBQztDQUFBLEFBWEQsSUFXQztTQVhZLGdCQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdib3h4LWtwaS1jZWxsJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2twaS1jZWxsLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4va3BpLWNlbGwuY29tcG9uZW50LnNjc3MnXSxcbn0pXG5leHBvcnQgY2xhc3MgS3BpQ2VsbENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIEBJbnB1dCgpIGNsYXNzTmFtZTogc3RyaW5nO1xuICBASW5wdXQoKSBpY29uTmFtZTogc3RyaW5nO1xuICBASW5wdXQoKSBpY29uU3JjOiBzdHJpbmc7XG4gIEBJbnB1dCgpIGxhYmVsOiBzdHJpbmc7XG4gIEBJbnB1dCgpIHZhbHVlOiBudW1iZXI7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpIHt9XG5cbn1cbiJdfQ==