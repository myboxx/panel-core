import { __awaiter, __decorate, __generator } from "tslib";
import { Component, Input } from '@angular/core';
import { File } from '@ionic-native/file/ngx';
import { ActionSheetController, AlertController, LoadingController, Platform, PopoverController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
var QrPopoverComponent = /** @class */ (function () {
    function QrPopoverComponent(platform, popoverCtrl, file, loaderCtrl, alertCtrl, actionSheetController, translate) {
        var _this = this;
        this.platform = platform;
        this.popoverCtrl = popoverCtrl;
        this.file = file;
        this.loaderCtrl = loaderCtrl;
        this.alertCtrl = alertCtrl;
        this.actionSheetController = actionSheetController;
        this.translate = translate;
        this.isCordovaOrCapacitor = false;
        this.elementType = 'url';
        this.destroyed$ = new Subject();
        this.translate.get(['GENERAL', 'QR'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe(function (t) { return _this.translations = t; });
        this.platform.ready().then(function () {
            _this.isCordovaOrCapacitor = _this.platform.is('cordova') || _this.platform.is('capacitor');
            if (_this.platform.is('android')) {
                _this.isAndroid = true;
                _this.folderpath = _this.file.externalRootDirectory;
            }
            else {
                _this.isAndroid = false;
                _this.folderpath = _this.file.documentsDirectory;
            }
        });
    }
    QrPopoverComponent.prototype.ngOnInit = function () {
        setTimeout(function () {
            var qrMainContainer = document.getElementsByTagName('ngx-qrcode')[0];
            if (!qrMainContainer) {
                return console.error('No hay contenedor para la imagen QR');
            }
            var qrImgContainer = qrMainContainer.firstElementChild;
            if (!qrImgContainer) {
                return console.error('No hay imagen QR');
            }
            var child = qrImgContainer.firstElementChild;
            if (child) {
                var src = child.getAttribute('src');
                if (src) {
                    var a = document.getElementById('downladableQr'); // Create <a>
                    a.setAttribute('src', src);
                }
                else {
                    console.error('--- No img src found');
                }
            }
            else {
                console.error('--- Element not found');
            }
        }, 900);
    };
    QrPopoverComponent.prototype.close = function () {
        this.popoverCtrl.dismiss();
    };
    QrPopoverComponent.prototype.saveImage = function (src) {
        return __awaiter(this, void 0, void 0, function () {
            var option, actionSheet, loader, scaledImg, block, dataType, realData, filename;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            header: this.translations.GENERAL.ACTION.options,
                            buttons: [{
                                    text: '500 x 500',
                                    handler: function () { option = 500; }
                                },
                                {
                                    text: '700 x 700',
                                    handler: function () { option = 700; }
                                },
                                {
                                    text: '1200 x 1200',
                                    handler: function () { option = 1200; }
                                }]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        actionSheet.present();
                        return [4 /*yield*/, actionSheet.onDidDismiss()];
                    case 2:
                        _a.sent();
                        if (!option) {
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this.loaderCtrl.create()];
                    case 3:
                        loader = _a.sent();
                        loader.present();
                        return [4 /*yield*/, this.scaleImage(src.getAttribute('src'), option, option)
                                .catch(function (error) {
                                _this.alertCtrl.create({
                                    header: _this.translations.GENERAL.error,
                                    message: _this.translations.QR.scalingError + '<br>' + JSON.stringify(error),
                                    buttons: [{ text: _this.translations.GENERAL.ACTION.ok }]
                                });
                                loader.dismiss();
                            })];
                    case 4:
                        scaledImg = _a.sent();
                        if (!scaledImg) {
                            return [2 /*return*/];
                        }
                        block = (scaledImg || '').split(';');
                        dataType = block[0].split(':')[1];
                        realData = block[1].split(',')[1];
                        filename = "MY_WEBSITE_" + option + "x" + option + "." + dataType.split('/')[1];
                        this.saveBase64(this.folderpath, realData, filename, dataType).then(function (path) { return __awaiter(_this, void 0, void 0, function () {
                            var _a, _b, _c;
                            return __generator(this, function (_d) {
                                switch (_d.label) {
                                    case 0:
                                        this.close();
                                        _b = (_a = this.alertCtrl).create;
                                        _c = {
                                            header: this.translations.QR.completed
                                        };
                                        return [4 /*yield*/, this.translate.get('QR.fileDownloadedOk', { filename: filename }).toPromise()];
                                    case 1:
                                        _b.apply(_a, [(_c.message = _d.sent(),
                                                _c.buttons = [{ text: this.translations.GENERAL.ACTION.ok }],
                                                _c)]).then(function (a) { return a.present(); });
                                        return [2 /*return*/];
                                }
                            });
                        }); }, function (error) {
                            console.error('saveBase64: error', error);
                            _this.alertCtrl.create({
                                header: _this.translations.GENERAL.error,
                                message: _this.translations.QR.fileDownloadError + '<br>' + JSON.stringify(error),
                                buttons: [{ text: _this.translations.GENERAL.ACTION.ok }]
                            }).then(function (a) { return a.present(); });
                        }).finally(function () { return _this.loaderCtrl.dismiss(); });
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Convert a base64 string in a Blob according to the data and contentType.
     *
     * @param b64Data Pure base64 string without contentType
     * @param contentType the content type of the file i.e (image/jpeg - image/png - text/plain)
     * @param sliceSize SliceSize to process the byteCharacters
     * @see http://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
     * @return Blob
     */
    QrPopoverComponent.prototype.b64toBlob = function (b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    };
    QrPopoverComponent.prototype.saveBase64 = function (pictureDir, content, name, dataType) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var blob = _this.b64toBlob(content, dataType);
            if (!_this.isCordovaOrCapacitor) {
                _this.downloadFile(blob, name, dataType);
                return resolve(pictureDir + name);
            }
            _this.file.writeFile(pictureDir, name, blob, { replace: true })
                .then(function () { return resolve(pictureDir + name); })
                .catch(function (err) {
                console.error('error writing blob. ERROR: ', err);
                reject(err);
            });
        });
    };
    QrPopoverComponent.prototype.scaleImage = function (base64Data, width, height) {
        return new Promise(function (resolve, reject) {
            var img = new Image();
            img.onload = function () {
                // We create a canvas and get its context.
                var canvas = document.createElement('canvas');
                var ctx = canvas.getContext('2d');
                // We set the dimensions at the wanted size.
                canvas.width = width;
                canvas.height = height;
                // We resize the image with the canvas method drawImage();
                ctx.drawImage(img, 0, 0, width, height);
                resolve(canvas.toDataURL());
            };
            img.onerror = (function (e) {
                reject(e.toString());
            });
            img.src = base64Data;
        });
    };
    // source: https://davidwalsh.name/javascript-download
    QrPopoverComponent.prototype.downloadFile = function (data, fileName, type) {
        if (type === void 0) { type = 'text/plain'; }
        // Create an invisible A element
        var a = document.createElement('a');
        a.style.display = 'none';
        document.body.appendChild(a);
        // Set the HREF to a Blob representation of the data to be downloaded
        a.href = window.URL.createObjectURL(new Blob([data], { type: type }));
        // Use download attribute to set set desired file name
        a.setAttribute('download', fileName);
        // Trigger the download by simulating click
        a.click();
        // Cleanup
        window.URL.revokeObjectURL(a.href);
        document.body.removeChild(a);
    };
    QrPopoverComponent.ctorParameters = function () { return [
        { type: Platform },
        { type: PopoverController },
        { type: File },
        { type: LoadingController },
        { type: AlertController },
        { type: ActionSheetController },
        { type: TranslateService }
    ]; };
    __decorate([
        Input()
    ], QrPopoverComponent.prototype, "websiteUrlString", void 0);
    QrPopoverComponent = __decorate([
        Component({
            selector: 'boxx-qr-popover',
            template: "<ion-content class=\"ion-padding-bottom\">\n    <div class=\"qr-toolbar\">\n        <ion-button fill=\"clear\" (click)=\"close()\">\n            <ion-icon name=\"close-circle-outline\"></ion-icon>\n        </ion-button>\n    </div>\n    <div class=\"ion-text-center\" *ngIf=\"websiteUrlString\">\n        <ngx-qrcode [elementType]=\"elementType\" [value]=\"websiteUrlString\" cssClass=\"qr-container\"\n            errorCorrectionLevel=\"L\">\n        </ngx-qrcode>\n\n        <ion-button color=\"primary\" id=\"downladableQr\" (click)=\"saveImage($event.target)\" translate>\n            GENERAL.ACTION.download\n        </ion-button>\n\n    </div>\n</ion-content>",
            styles: [":host ion-content{--background:white}:host .qr-toolbar{text-align:right}"]
        })
    ], QrPopoverComponent);
    return QrPopoverComponent;
}());
export { QrPopoverComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicXItcG9wb3Zlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9wYW5lbC1jb3JlLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvcXItY29tcG9uZW50L3FyLXBvcG92ZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBVSxNQUFNLGVBQWUsQ0FBQztBQUN6RCxPQUFPLEVBQUUsSUFBSSxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDOUMsT0FBTyxFQUFFLHFCQUFxQixFQUFFLGVBQWUsRUFBRSxpQkFBaUIsRUFBRSxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN4SCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUN2RCxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQy9CLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQU8zQztJQUdJLDRCQUNXLFFBQWtCLEVBQ2pCLFdBQThCLEVBQzlCLElBQVUsRUFDVixVQUE2QixFQUM3QixTQUEwQixFQUMxQixxQkFBNEMsRUFDNUMsU0FBMkI7UUFQdkMsaUJBMEJDO1FBekJVLGFBQVEsR0FBUixRQUFRLENBQVU7UUFDakIsZ0JBQVcsR0FBWCxXQUFXLENBQW1CO1FBQzlCLFNBQUksR0FBSixJQUFJLENBQU07UUFDVixlQUFVLEdBQVYsVUFBVSxDQUFtQjtRQUM3QixjQUFTLEdBQVQsU0FBUyxDQUFpQjtRQUMxQiwwQkFBcUIsR0FBckIscUJBQXFCLENBQXVCO1FBQzVDLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBcUJ2Qyx5QkFBb0IsR0FBRyxLQUFLLENBQUM7UUFFN0IsZ0JBQVcsR0FBRyxLQUFLLENBQUM7UUFNcEIsZUFBVSxHQUFHLElBQUksT0FBTyxFQUFXLENBQUM7UUExQmhDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQ2hDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ2hDLFNBQVMsQ0FBQyxVQUFDLENBQU0sSUFBSyxPQUFBLEtBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixDQUFDLENBQUM7UUFFbEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxJQUFJLENBQUM7WUFDdkIsS0FBSSxDQUFDLG9CQUFvQixHQUFHLEtBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBRXpGLElBQUksS0FBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLEVBQUU7Z0JBQzdCLEtBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO2dCQUN0QixLQUFJLENBQUMsVUFBVSxHQUFHLEtBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUM7YUFDckQ7aUJBQ0k7Z0JBQ0QsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7Z0JBQ3ZCLEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQzthQUNsRDtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQVlELHFDQUFRLEdBQVI7UUFDSSxVQUFVLENBQUM7WUFDUCxJQUFNLGVBQWUsR0FBRyxRQUFRLENBQUMsb0JBQW9CLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFdkUsSUFBSSxDQUFDLGVBQWUsRUFBRTtnQkFBRSxPQUFPLE9BQU8sQ0FBQyxLQUFLLENBQUMscUNBQXFDLENBQUMsQ0FBQzthQUFFO1lBRXRGLElBQU0sY0FBYyxHQUFHLGVBQWUsQ0FBQyxpQkFBaUIsQ0FBQztZQUV6RCxJQUFJLENBQUMsY0FBYyxFQUFFO2dCQUFFLE9BQU8sT0FBTyxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2FBQUU7WUFHbEUsSUFBTSxLQUFLLEdBQUcsY0FBYyxDQUFDLGlCQUFpQixDQUFDO1lBRS9DLElBQUksS0FBSyxFQUFFO2dCQUNQLElBQU0sR0FBRyxHQUFHLEtBQUssQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBRXRDLElBQUksR0FBRyxFQUFFO29CQUNMLElBQU0sQ0FBQyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxhQUFhO29CQUNqRSxDQUFDLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztpQkFDOUI7cUJBQ0k7b0JBQ0QsT0FBTyxDQUFDLEtBQUssQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO2lCQUN6QzthQUNKO2lCQUNJO2dCQUNELE9BQU8sQ0FBQyxLQUFLLENBQUMsdUJBQXVCLENBQUMsQ0FBQzthQUMxQztRQUNMLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNaLENBQUM7SUFFRCxrQ0FBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBRUssc0NBQVMsR0FBZixVQUFnQixHQUFnQjs7Ozs7OzRCQUVSLHFCQUFNLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUM7NEJBQ3hELE1BQU0sRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsT0FBTzs0QkFDaEQsT0FBTyxFQUFFLENBQUM7b0NBQ04sSUFBSSxFQUFFLFdBQVc7b0NBQ2pCLE9BQU8sRUFBRSxjQUFRLE1BQU0sR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO2lDQUNuQztnQ0FDRDtvQ0FDSSxJQUFJLEVBQUUsV0FBVztvQ0FDakIsT0FBTyxFQUFFLGNBQVEsTUFBTSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7aUNBQ25DO2dDQUNEO29DQUNJLElBQUksRUFBRSxhQUFhO29DQUNuQixPQUFPLEVBQUUsY0FBUSxNQUFNLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQztpQ0FDcEMsQ0FBQzt5QkFDTCxDQUFDLEVBQUE7O3dCQWRJLFdBQVcsR0FBRyxTQWNsQjt3QkFFRixXQUFXLENBQUMsT0FBTyxFQUFFLENBQUM7d0JBRXRCLHFCQUFNLFdBQVcsQ0FBQyxZQUFZLEVBQUUsRUFBQTs7d0JBQWhDLFNBQWdDLENBQUM7d0JBRWpDLElBQUksQ0FBQyxNQUFNLEVBQUU7NEJBQUUsc0JBQU87eUJBQUU7d0JBR1QscUJBQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsRUFBQTs7d0JBQXZDLE1BQU0sR0FBRyxTQUE4Qjt3QkFFN0MsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO3dCQUVDLHFCQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDO2lDQUMzRSxLQUFLLENBQUMsVUFBQSxLQUFLO2dDQUNSLEtBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO29DQUNsQixNQUFNLEVBQUUsS0FBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsS0FBSztvQ0FDdkMsT0FBTyxFQUFFLEtBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFlBQVksR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUM7b0NBQzNFLE9BQU8sRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLEtBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsQ0FBQztpQ0FDM0QsQ0FBQyxDQUFDO2dDQUVILE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQzs0QkFDckIsQ0FBQyxDQUFDLEVBQUE7O3dCQVRBLFNBQVMsR0FBRyxTQVNaO3dCQUVOLElBQUksQ0FBQyxTQUFTLEVBQUU7NEJBQUUsc0JBQU87eUJBQUU7d0JBR3JCLEtBQUssR0FBRyxDQUFDLFNBQVMsSUFBSSxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBR3JDLFFBQVEsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUdsQyxRQUFRLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFHbEMsUUFBUSxHQUFHLGdCQUFjLE1BQU0sU0FBSSxNQUFNLFNBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUcsQ0FBQzt3QkFFNUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQU0sSUFBSTs7Ozs7d0NBQzFFLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQzt3Q0FFYixLQUFBLENBQUEsS0FBQSxJQUFJLENBQUMsU0FBUyxDQUFBLENBQUMsTUFBTSxDQUFBOzs0Q0FDakIsTUFBTSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFNBQVM7O3dDQUM3QixxQkFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxFQUFFLFFBQVEsVUFBQSxFQUFFLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBQTs7d0NBRnRGLGVBRUksVUFBTyxHQUFFLFNBQXlFO2dEQUNsRixVQUFPLEdBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLENBQUM7cURBQzFELENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLE9BQU8sRUFBRSxFQUFYLENBQVcsQ0FBQyxDQUFDOzs7OzZCQUM3QixFQUFFLFVBQUEsS0FBSzs0QkFDSixPQUFPLENBQUMsS0FBSyxDQUFDLG1CQUFtQixFQUFFLEtBQUssQ0FBQyxDQUFDOzRCQUMxQyxLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQztnQ0FDbEIsTUFBTSxFQUFFLEtBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLEtBQUs7Z0NBQ3ZDLE9BQU8sRUFBRSxLQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUM7Z0NBQ2hGLE9BQU8sRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLEtBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsQ0FBQzs2QkFDM0QsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBWCxDQUFXLENBQUMsQ0FBQzt3QkFDOUIsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLGNBQU0sT0FBQSxLQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxFQUF6QixDQUF5QixDQUFDLENBQUM7Ozs7O0tBQy9DO0lBRUQ7Ozs7Ozs7O09BUUc7SUFDSCxzQ0FBUyxHQUFULFVBQVUsT0FBTyxFQUFFLFdBQVcsRUFBRSxTQUFVO1FBQ3RDLFdBQVcsR0FBRyxXQUFXLElBQUksRUFBRSxDQUFDO1FBQ2hDLFNBQVMsR0FBRyxTQUFTLElBQUksR0FBRyxDQUFDO1FBRTdCLElBQU0sY0FBYyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNyQyxJQUFNLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFFdEIsS0FBSyxJQUFJLE1BQU0sR0FBRyxDQUFDLEVBQUUsTUFBTSxHQUFHLGNBQWMsQ0FBQyxNQUFNLEVBQUUsTUFBTSxJQUFJLFNBQVMsRUFBRTtZQUN0RSxJQUFNLEtBQUssR0FBRyxjQUFjLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxNQUFNLEdBQUcsU0FBUyxDQUFDLENBQUM7WUFFL0QsSUFBTSxXQUFXLEdBQUcsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzVDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNuQyxXQUFXLENBQUMsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUN4QztZQUVELElBQU0sU0FBUyxHQUFHLElBQUksVUFBVSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBRTlDLFVBQVUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDOUI7UUFFRCxJQUFNLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLENBQUMsQ0FBQztRQUN6RCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRU0sdUNBQVUsR0FBakIsVUFBa0IsVUFBa0IsRUFBRSxPQUFlLEVBQUUsSUFBWSxFQUFFLFFBQWdCO1FBQXJGLGlCQWlCQztRQWhCRyxPQUFPLElBQUksT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFDL0IsSUFBTSxJQUFJLEdBQUcsS0FBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFHL0MsSUFBSSxDQUFDLEtBQUksQ0FBQyxvQkFBb0IsRUFBQztnQkFDM0IsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QyxPQUFPLE9BQU8sQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLENBQUM7YUFDckM7WUFFRCxLQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsQ0FBQztpQkFDekQsSUFBSSxDQUFDLGNBQU0sT0FBQSxPQUFPLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxFQUExQixDQUEwQixDQUFDO2lCQUN0QyxLQUFLLENBQUMsVUFBQyxHQUFHO2dCQUNQLE9BQU8sQ0FBQyxLQUFLLENBQUMsNkJBQTZCLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ2xELE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNoQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHVDQUFVLEdBQVYsVUFBVyxVQUFrQixFQUFFLEtBQWEsRUFBRSxNQUFjO1FBQ3hELE9BQU8sSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUMvQixJQUFNLEdBQUcsR0FBRyxJQUFJLEtBQUssRUFBRSxDQUFDO1lBQ3hCLEdBQUcsQ0FBQyxNQUFNLEdBQUc7Z0JBQ1QsMENBQTBDO2dCQUMxQyxJQUFNLE1BQU0sR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUNoRCxJQUFNLEdBQUcsR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUVwQyw0Q0FBNEM7Z0JBQzVDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO2dCQUNyQixNQUFNLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztnQkFFdkIsMERBQTBEO2dCQUMxRCxHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQztnQkFFeEMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDO1lBQ2hDLENBQUMsQ0FBQztZQUVGLEdBQUcsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxVQUFDLENBQUM7Z0JBQ2IsTUFBTSxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1lBQ3pCLENBQUMsQ0FBQyxDQUFDO1lBRUgsR0FBRyxDQUFDLEdBQUcsR0FBRyxVQUFVLENBQUM7UUFDekIsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsc0RBQXNEO0lBQ3RELHlDQUFZLEdBQVosVUFBYSxJQUFJLEVBQUUsUUFBUSxFQUFFLElBQW1CO1FBQW5CLHFCQUFBLEVBQUEsbUJBQW1CO1FBQzVDLGdDQUFnQztRQUNoQyxJQUFNLENBQUMsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3RDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztRQUN6QixRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUU3QixxRUFBcUU7UUFDckUsQ0FBQyxDQUFDLElBQUksR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FDL0IsSUFBSSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FDN0IsQ0FBQztRQUVGLHNEQUFzRDtRQUN0RCxDQUFDLENBQUMsWUFBWSxDQUFDLFVBQVUsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUVyQywyQ0FBMkM7UUFDM0MsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBRVYsVUFBVTtRQUNWLE1BQU0sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNuQyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNqQyxDQUFDOztnQkFsUG9CLFFBQVE7Z0JBQ0osaUJBQWlCO2dCQUN4QixJQUFJO2dCQUNFLGlCQUFpQjtnQkFDbEIsZUFBZTtnQkFDSCxxQkFBcUI7Z0JBQ2pDLGdCQUFnQjs7SUFUOUI7UUFBUixLQUFLLEVBQUU7Z0VBQTBCO0lBRHpCLGtCQUFrQjtRQUw5QixTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsaUJBQWlCO1lBQzNCLHFxQkFBMEM7O1NBRTdDLENBQUM7T0FDVyxrQkFBa0IsQ0F1UDlCO0lBQUQseUJBQUM7Q0FBQSxBQXZQRCxJQXVQQztTQXZQWSxrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZpbGUgfSBmcm9tICdAaW9uaWMtbmF0aXZlL2ZpbGUvbmd4JztcbmltcG9ydCB7IEFjdGlvblNoZWV0Q29udHJvbGxlciwgQWxlcnRDb250cm9sbGVyLCBMb2FkaW5nQ29udHJvbGxlciwgUGxhdGZvcm0sIFBvcG92ZXJDb250cm9sbGVyIH0gZnJvbSAnQGlvbmljL2FuZ3VsYXInO1xuaW1wb3J0IHsgVHJhbnNsYXRlU2VydmljZSB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2JveHgtcXItcG9wb3ZlcicsXG4gICAgdGVtcGxhdGVVcmw6ICcuL3FyLXBvcG92ZXIuY29tcG9uZW50Lmh0bWwnLFxuICAgIHN0eWxlVXJsczogWycuL3FyLXBvcG92ZXIuY29tcG9uZW50LnNjc3MnXSxcbn0pXG5leHBvcnQgY2xhc3MgUXJQb3BvdmVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICBASW5wdXQoKSB3ZWJzaXRlVXJsU3RyaW5nOiBzdHJpbmc7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHVibGljIHBsYXRmb3JtOiBQbGF0Zm9ybSxcbiAgICAgICAgcHJpdmF0ZSBwb3BvdmVyQ3RybDogUG9wb3ZlckNvbnRyb2xsZXIsXG4gICAgICAgIHByaXZhdGUgZmlsZTogRmlsZSxcbiAgICAgICAgcHJpdmF0ZSBsb2FkZXJDdHJsOiBMb2FkaW5nQ29udHJvbGxlcixcbiAgICAgICAgcHJpdmF0ZSBhbGVydEN0cmw6IEFsZXJ0Q29udHJvbGxlcixcbiAgICAgICAgcHJpdmF0ZSBhY3Rpb25TaGVldENvbnRyb2xsZXI6IEFjdGlvblNoZWV0Q29udHJvbGxlcixcbiAgICAgICAgcHJpdmF0ZSB0cmFuc2xhdGU6IFRyYW5zbGF0ZVNlcnZpY2VcbiAgICApIHtcblxuICAgICAgICB0aGlzLnRyYW5zbGF0ZS5nZXQoWydHRU5FUkFMJywgJ1FSJ10pXG4gICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5kZXN0cm95ZWQkKSlcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoKHQ6IGFueSkgPT4gdGhpcy50cmFuc2xhdGlvbnMgPSB0KTtcblxuICAgICAgICB0aGlzLnBsYXRmb3JtLnJlYWR5KCkudGhlbigoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmlzQ29yZG92YU9yQ2FwYWNpdG9yID0gdGhpcy5wbGF0Zm9ybS5pcygnY29yZG92YScpIHx8IHRoaXMucGxhdGZvcm0uaXMoJ2NhcGFjaXRvcicpO1xuXG4gICAgICAgICAgICBpZiAodGhpcy5wbGF0Zm9ybS5pcygnYW5kcm9pZCcpKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5pc0FuZHJvaWQgPSB0cnVlO1xuICAgICAgICAgICAgICAgIHRoaXMuZm9sZGVycGF0aCA9IHRoaXMuZmlsZS5leHRlcm5hbFJvb3REaXJlY3Rvcnk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLmlzQW5kcm9pZCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIHRoaXMuZm9sZGVycGF0aCA9IHRoaXMuZmlsZS5kb2N1bWVudHNEaXJlY3Rvcnk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGlzQ29yZG92YU9yQ2FwYWNpdG9yID0gZmFsc2U7XG5cbiAgICBlbGVtZW50VHlwZSA9ICd1cmwnO1xuICAgIGZvbGRlcnBhdGg6IHN0cmluZztcbiAgICBpc0FuZHJvaWQ6IGJvb2xlYW47XG5cbiAgICB0cmFuc2xhdGlvbnM6IGFueTtcblxuICAgIGRlc3Ryb3llZCQgPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgY29uc3QgcXJNYWluQ29udGFpbmVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ25neC1xcmNvZGUnKVswXTtcblxuICAgICAgICAgICAgaWYgKCFxck1haW5Db250YWluZXIpIHsgcmV0dXJuIGNvbnNvbGUuZXJyb3IoJ05vIGhheSBjb250ZW5lZG9yIHBhcmEgbGEgaW1hZ2VuIFFSJyk7IH1cblxuICAgICAgICAgICAgY29uc3QgcXJJbWdDb250YWluZXIgPSBxck1haW5Db250YWluZXIuZmlyc3RFbGVtZW50Q2hpbGQ7XG5cbiAgICAgICAgICAgIGlmICghcXJJbWdDb250YWluZXIpIHsgcmV0dXJuIGNvbnNvbGUuZXJyb3IoJ05vIGhheSBpbWFnZW4gUVInKTsgfVxuXG5cbiAgICAgICAgICAgIGNvbnN0IGNoaWxkID0gcXJJbWdDb250YWluZXIuZmlyc3RFbGVtZW50Q2hpbGQ7XG5cbiAgICAgICAgICAgIGlmIChjaGlsZCkge1xuICAgICAgICAgICAgICAgIGNvbnN0IHNyYyA9IGNoaWxkLmdldEF0dHJpYnV0ZSgnc3JjJyk7XG5cbiAgICAgICAgICAgICAgICBpZiAoc3JjKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGEgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnZG93bmxhZGFibGVRcicpOyAvLyBDcmVhdGUgPGE+XG4gICAgICAgICAgICAgICAgICAgIGEuc2V0QXR0cmlidXRlKCdzcmMnLCBzcmMpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcignLS0tIE5vIGltZyBzcmMgZm91bmQnKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKCctLS0gRWxlbWVudCBub3QgZm91bmQnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwgOTAwKTtcbiAgICB9XG5cbiAgICBjbG9zZSgpIHtcbiAgICAgICAgdGhpcy5wb3BvdmVyQ3RybC5kaXNtaXNzKCk7XG4gICAgfVxuXG4gICAgYXN5bmMgc2F2ZUltYWdlKHNyYzogSFRNTEVsZW1lbnQpIHtcbiAgICAgICAgbGV0IG9wdGlvbjogNTAwIHwgNzAwIHwgMTIwMDtcbiAgICAgICAgY29uc3QgYWN0aW9uU2hlZXQgPSBhd2FpdCB0aGlzLmFjdGlvblNoZWV0Q29udHJvbGxlci5jcmVhdGUoe1xuICAgICAgICAgICAgaGVhZGVyOiB0aGlzLnRyYW5zbGF0aW9ucy5HRU5FUkFMLkFDVElPTi5vcHRpb25zLFxuICAgICAgICAgICAgYnV0dG9uczogW3tcbiAgICAgICAgICAgICAgICB0ZXh0OiAnNTAwIHggNTAwJyxcbiAgICAgICAgICAgICAgICBoYW5kbGVyOiAoKSA9PiB7IG9wdGlvbiA9IDUwMDsgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICB0ZXh0OiAnNzAwIHggNzAwJyxcbiAgICAgICAgICAgICAgICBoYW5kbGVyOiAoKSA9PiB7IG9wdGlvbiA9IDcwMDsgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICB0ZXh0OiAnMTIwMCB4IDEyMDAnLFxuICAgICAgICAgICAgICAgIGhhbmRsZXI6ICgpID0+IHsgb3B0aW9uID0gMTIwMDsgfVxuICAgICAgICAgICAgfV1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgYWN0aW9uU2hlZXQucHJlc2VudCgpO1xuXG4gICAgICAgIGF3YWl0IGFjdGlvblNoZWV0Lm9uRGlkRGlzbWlzcygpO1xuXG4gICAgICAgIGlmICghb3B0aW9uKSB7IHJldHVybjsgfVxuXG5cbiAgICAgICAgY29uc3QgbG9hZGVyID0gYXdhaXQgdGhpcy5sb2FkZXJDdHJsLmNyZWF0ZSgpO1xuXG4gICAgICAgIGxvYWRlci5wcmVzZW50KCk7XG5cbiAgICAgICAgY29uc3Qgc2NhbGVkSW1nID0gYXdhaXQgdGhpcy5zY2FsZUltYWdlKHNyYy5nZXRBdHRyaWJ1dGUoJ3NyYycpLCBvcHRpb24sIG9wdGlvbilcbiAgICAgICAgICAgIC5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5hbGVydEN0cmwuY3JlYXRlKHtcbiAgICAgICAgICAgICAgICAgICAgaGVhZGVyOiB0aGlzLnRyYW5zbGF0aW9ucy5HRU5FUkFMLmVycm9yLFxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiB0aGlzLnRyYW5zbGF0aW9ucy5RUi5zY2FsaW5nRXJyb3IgKyAnPGJyPicgKyBKU09OLnN0cmluZ2lmeShlcnJvciksXG4gICAgICAgICAgICAgICAgICAgIGJ1dHRvbnM6IFt7IHRleHQ6IHRoaXMudHJhbnNsYXRpb25zLkdFTkVSQUwuQUNUSU9OLm9rIH1dXG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICBsb2FkZXIuZGlzbWlzcygpO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgaWYgKCFzY2FsZWRJbWcpIHsgcmV0dXJuOyB9XG5cblxuICAgICAgICBjb25zdCBibG9jayA9IChzY2FsZWRJbWcgfHwgJycpLnNwbGl0KCc7Jyk7XG5cbiAgICAgICAgLy8gR2V0IHRoZSBjb250ZW50IHR5cGVcbiAgICAgICAgY29uc3QgZGF0YVR5cGUgPSBibG9ja1swXS5zcGxpdCgnOicpWzFdO1xuXG4gICAgICAgIC8vIGdldCB0aGUgcmVhbCBiYXNlNjQgY29udGVudCBvZiB0aGUgZmlsZVxuICAgICAgICBjb25zdCByZWFsRGF0YSA9IGJsb2NrWzFdLnNwbGl0KCcsJylbMV07XG5cbiAgICAgICAgLy8gVGhlIG5hbWUgb2YgeW91ciBmaWxlLCBub3RlIHRoYXQgeW91IG5lZWQgdG8ga25vdyBpZiBpcyAucG5nLC5qcGVnIGV0Y1xuICAgICAgICBjb25zdCBmaWxlbmFtZSA9IGBNWV9XRUJTSVRFXyR7b3B0aW9ufXgke29wdGlvbn0uJHtkYXRhVHlwZS5zcGxpdCgnLycpWzFdfWA7XG5cbiAgICAgICAgdGhpcy5zYXZlQmFzZTY0KHRoaXMuZm9sZGVycGF0aCwgcmVhbERhdGEsIGZpbGVuYW1lLCBkYXRhVHlwZSkudGhlbihhc3luYyBwYXRoID0+IHtcbiAgICAgICAgICAgIHRoaXMuY2xvc2UoKTtcblxuICAgICAgICAgICAgdGhpcy5hbGVydEN0cmwuY3JlYXRlKHtcbiAgICAgICAgICAgICAgICBoZWFkZXI6IHRoaXMudHJhbnNsYXRpb25zLlFSLmNvbXBsZXRlZCxcbiAgICAgICAgICAgICAgICBtZXNzYWdlOiBhd2FpdCB0aGlzLnRyYW5zbGF0ZS5nZXQoJ1FSLmZpbGVEb3dubG9hZGVkT2snLCB7IGZpbGVuYW1lIH0pLnRvUHJvbWlzZSgpLFxuICAgICAgICAgICAgICAgIGJ1dHRvbnM6IFt7IHRleHQ6IHRoaXMudHJhbnNsYXRpb25zLkdFTkVSQUwuQUNUSU9OLm9rIH1dXG4gICAgICAgICAgICB9KS50aGVuKGEgPT4gYS5wcmVzZW50KCkpO1xuICAgICAgICB9LCBlcnJvciA9PiB7XG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdzYXZlQmFzZTY0OiBlcnJvcicsIGVycm9yKTtcbiAgICAgICAgICAgIHRoaXMuYWxlcnRDdHJsLmNyZWF0ZSh7XG4gICAgICAgICAgICAgICAgaGVhZGVyOiB0aGlzLnRyYW5zbGF0aW9ucy5HRU5FUkFMLmVycm9yLFxuICAgICAgICAgICAgICAgIG1lc3NhZ2U6IHRoaXMudHJhbnNsYXRpb25zLlFSLmZpbGVEb3dubG9hZEVycm9yICsgJzxicj4nICsgSlNPTi5zdHJpbmdpZnkoZXJyb3IpLFxuICAgICAgICAgICAgICAgIGJ1dHRvbnM6IFt7IHRleHQ6IHRoaXMudHJhbnNsYXRpb25zLkdFTkVSQUwuQUNUSU9OLm9rIH1dXG4gICAgICAgICAgICB9KS50aGVuKGEgPT4gYS5wcmVzZW50KCkpO1xuICAgICAgICB9KS5maW5hbGx5KCgpID0+IHRoaXMubG9hZGVyQ3RybC5kaXNtaXNzKCkpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENvbnZlcnQgYSBiYXNlNjQgc3RyaW5nIGluIGEgQmxvYiBhY2NvcmRpbmcgdG8gdGhlIGRhdGEgYW5kIGNvbnRlbnRUeXBlLlxuICAgICAqXG4gICAgICogQHBhcmFtIGI2NERhdGEgUHVyZSBiYXNlNjQgc3RyaW5nIHdpdGhvdXQgY29udGVudFR5cGVcbiAgICAgKiBAcGFyYW0gY29udGVudFR5cGUgdGhlIGNvbnRlbnQgdHlwZSBvZiB0aGUgZmlsZSBpLmUgKGltYWdlL2pwZWcgLSBpbWFnZS9wbmcgLSB0ZXh0L3BsYWluKVxuICAgICAqIEBwYXJhbSBzbGljZVNpemUgU2xpY2VTaXplIHRvIHByb2Nlc3MgdGhlIGJ5dGVDaGFyYWN0ZXJzXG4gICAgICogQHNlZSBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzE2MjQ1NzY3L2NyZWF0aW5nLWEtYmxvYi1mcm9tLWEtYmFzZTY0LXN0cmluZy1pbi1qYXZhc2NyaXB0XG4gICAgICogQHJldHVybiBCbG9iXG4gICAgICovXG4gICAgYjY0dG9CbG9iKGI2NERhdGEsIGNvbnRlbnRUeXBlLCBzbGljZVNpemU/KSB7XG4gICAgICAgIGNvbnRlbnRUeXBlID0gY29udGVudFR5cGUgfHwgJyc7XG4gICAgICAgIHNsaWNlU2l6ZSA9IHNsaWNlU2l6ZSB8fCA1MTI7XG5cbiAgICAgICAgY29uc3QgYnl0ZUNoYXJhY3RlcnMgPSBhdG9iKGI2NERhdGEpO1xuICAgICAgICBjb25zdCBieXRlQXJyYXlzID0gW107XG5cbiAgICAgICAgZm9yIChsZXQgb2Zmc2V0ID0gMDsgb2Zmc2V0IDwgYnl0ZUNoYXJhY3RlcnMubGVuZ3RoOyBvZmZzZXQgKz0gc2xpY2VTaXplKSB7XG4gICAgICAgICAgICBjb25zdCBzbGljZSA9IGJ5dGVDaGFyYWN0ZXJzLnNsaWNlKG9mZnNldCwgb2Zmc2V0ICsgc2xpY2VTaXplKTtcblxuICAgICAgICAgICAgY29uc3QgYnl0ZU51bWJlcnMgPSBuZXcgQXJyYXkoc2xpY2UubGVuZ3RoKTtcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgc2xpY2UubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICBieXRlTnVtYmVyc1tpXSA9IHNsaWNlLmNoYXJDb2RlQXQoaSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGNvbnN0IGJ5dGVBcnJheSA9IG5ldyBVaW50OEFycmF5KGJ5dGVOdW1iZXJzKTtcblxuICAgICAgICAgICAgYnl0ZUFycmF5cy5wdXNoKGJ5dGVBcnJheSk7XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBibG9iID0gbmV3IEJsb2IoYnl0ZUFycmF5cywgeyB0eXBlOiBjb250ZW50VHlwZSB9KTtcbiAgICAgICAgcmV0dXJuIGJsb2I7XG4gICAgfVxuXG4gICAgcHVibGljIHNhdmVCYXNlNjQocGljdHVyZURpcjogc3RyaW5nLCBjb250ZW50OiBzdHJpbmcsIG5hbWU6IHN0cmluZywgZGF0YVR5cGU6IHN0cmluZyk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAgICAgICBjb25zdCBibG9iID0gdGhpcy5iNjR0b0Jsb2IoY29udGVudCwgZGF0YVR5cGUpO1xuXG5cbiAgICAgICAgICAgIGlmICghdGhpcy5pc0NvcmRvdmFPckNhcGFjaXRvcil7XG4gICAgICAgICAgICAgICAgdGhpcy5kb3dubG9hZEZpbGUoYmxvYiwgbmFtZSwgZGF0YVR5cGUpO1xuICAgICAgICAgICAgICAgIHJldHVybiByZXNvbHZlKHBpY3R1cmVEaXIgKyBuYW1lKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy5maWxlLndyaXRlRmlsZShwaWN0dXJlRGlyLCBuYW1lLCBibG9iLCB7IHJlcGxhY2U6IHRydWUgfSlcbiAgICAgICAgICAgICAgICAudGhlbigoKSA9PiByZXNvbHZlKHBpY3R1cmVEaXIgKyBuYW1lKSlcbiAgICAgICAgICAgICAgICAuY2F0Y2goKGVycikgPT4ge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKCdlcnJvciB3cml0aW5nIGJsb2IuIEVSUk9SOiAnLCBlcnIpO1xuICAgICAgICAgICAgICAgICAgICByZWplY3QoZXJyKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgc2NhbGVJbWFnZShiYXNlNjREYXRhOiBzdHJpbmcsIHdpZHRoOiBudW1iZXIsIGhlaWdodDogbnVtYmVyKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IGltZyA9IG5ldyBJbWFnZSgpO1xuICAgICAgICAgICAgaW1nLm9ubG9hZCA9ICgpID0+IHtcbiAgICAgICAgICAgICAgICAvLyBXZSBjcmVhdGUgYSBjYW52YXMgYW5kIGdldCBpdHMgY29udGV4dC5cbiAgICAgICAgICAgICAgICBjb25zdCBjYW52YXMgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdjYW52YXMnKTtcbiAgICAgICAgICAgICAgICBjb25zdCBjdHggPSBjYW52YXMuZ2V0Q29udGV4dCgnMmQnKTtcblxuICAgICAgICAgICAgICAgIC8vIFdlIHNldCB0aGUgZGltZW5zaW9ucyBhdCB0aGUgd2FudGVkIHNpemUuXG4gICAgICAgICAgICAgICAgY2FudmFzLndpZHRoID0gd2lkdGg7XG4gICAgICAgICAgICAgICAgY2FudmFzLmhlaWdodCA9IGhlaWdodDtcblxuICAgICAgICAgICAgICAgIC8vIFdlIHJlc2l6ZSB0aGUgaW1hZ2Ugd2l0aCB0aGUgY2FudmFzIG1ldGhvZCBkcmF3SW1hZ2UoKTtcbiAgICAgICAgICAgICAgICBjdHguZHJhd0ltYWdlKGltZywgMCwgMCwgd2lkdGgsIGhlaWdodCk7XG5cbiAgICAgICAgICAgICAgICByZXNvbHZlKGNhbnZhcy50b0RhdGFVUkwoKSk7XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICBpbWcub25lcnJvciA9ICgoZSkgPT4ge1xuICAgICAgICAgICAgICAgIHJlamVjdChlLnRvU3RyaW5nKCkpO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIGltZy5zcmMgPSBiYXNlNjREYXRhO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICAvLyBzb3VyY2U6IGh0dHBzOi8vZGF2aWR3YWxzaC5uYW1lL2phdmFzY3JpcHQtZG93bmxvYWRcbiAgICBkb3dubG9hZEZpbGUoZGF0YSwgZmlsZU5hbWUsIHR5cGUgPSAndGV4dC9wbGFpbicpIHtcbiAgICAgICAgLy8gQ3JlYXRlIGFuIGludmlzaWJsZSBBIGVsZW1lbnRcbiAgICAgICAgY29uc3QgYSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2EnKTtcbiAgICAgICAgYS5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xuICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKGEpO1xuXG4gICAgICAgIC8vIFNldCB0aGUgSFJFRiB0byBhIEJsb2IgcmVwcmVzZW50YXRpb24gb2YgdGhlIGRhdGEgdG8gYmUgZG93bmxvYWRlZFxuICAgICAgICBhLmhyZWYgPSB3aW5kb3cuVVJMLmNyZWF0ZU9iamVjdFVSTChcbiAgICAgICAgICAgIG5ldyBCbG9iKFtkYXRhXSwgeyB0eXBlIH0pXG4gICAgICAgICk7XG5cbiAgICAgICAgLy8gVXNlIGRvd25sb2FkIGF0dHJpYnV0ZSB0byBzZXQgc2V0IGRlc2lyZWQgZmlsZSBuYW1lXG4gICAgICAgIGEuc2V0QXR0cmlidXRlKCdkb3dubG9hZCcsIGZpbGVOYW1lKTtcblxuICAgICAgICAvLyBUcmlnZ2VyIHRoZSBkb3dubG9hZCBieSBzaW11bGF0aW5nIGNsaWNrXG4gICAgICAgIGEuY2xpY2soKTtcblxuICAgICAgICAvLyBDbGVhbnVwXG4gICAgICAgIHdpbmRvdy5VUkwucmV2b2tlT2JqZWN0VVJMKGEuaHJlZik7XG4gICAgICAgIGRvY3VtZW50LmJvZHkucmVtb3ZlQ2hpbGQoYSk7XG4gICAgfVxufVxuIl19