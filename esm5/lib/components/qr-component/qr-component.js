import { __awaiter, __decorate, __generator } from "tslib";
import { Component, Input } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { Subject } from 'rxjs';
import { QrPopoverComponent } from './qr-popover.component';
var QrComponent = /** @class */ (function () {
    function QrComponent(popoverController) {
        this.popoverController = popoverController;
        this.destroyed$ = new Subject();
    }
    QrComponent.prototype.ngOnInit = function () {
    };
    QrComponent.prototype.ngOnDestroy = function () {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    };
    QrComponent.prototype.openQRPopover = function () {
        return __awaiter(this, void 0, void 0, function () {
            var popover;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.popoverController.create({
                            component: QrPopoverComponent,
                            componentProps: {
                                websiteUrlString: this.websiteUrl
                            }
                        })];
                    case 1:
                        popover = _a.sent();
                        return [4 /*yield*/, popover.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    QrComponent.ctorParameters = function () { return [
        { type: PopoverController }
    ]; };
    __decorate([
        Input()
    ], QrComponent.prototype, "isLoading$", void 0);
    __decorate([
        Input()
    ], QrComponent.prototype, "websiteUrl", void 0);
    QrComponent = __decorate([
        Component({
            selector: 'boxx-qr-component',
            template: "<div id=\"qr-spinner-container\" *ngIf=\"(isLoading$ | async) === true\">\n    <ion-spinner  id=\"qr-spinner\"></ion-spinner>\n</div>\n<div *ngIf=\"websiteUrl && (isLoading$ | async) === false\">\n    <ion-icon src=\"assets/icon/qr.svg\" slot=\"end\" (click)=\"openQRPopover()\">\n    </ion-icon>\n</div>\n",
            styles: ["#qr-spinner-container{width:100%;text-align:center}#qr-spinner-container ion-spinner{color:var(--ion-color-primary-contrast,#fff)}ion-icon{border-radius:5px;padding:5px;background:var(--ion-color-primary-contrast,#fff)}"]
        })
    ], QrComponent);
    return QrComponent;
}());
export { QrComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicXItY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvcGFuZWwtY29yZS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL3FyLWNvbXBvbmVudC9xci1jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFxQixNQUFNLGVBQWUsQ0FBQztBQUNwRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNuRCxPQUFPLEVBQWMsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBTzVEO0lBSUkscUJBQ1ksaUJBQW9DO1FBQXBDLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFJaEQsZUFBVSxHQUFHLElBQUksT0FBTyxFQUFXLENBQUM7SUFIaEMsQ0FBQztJQUtMLDhCQUFRLEdBQVI7SUFFQSxDQUFDO0lBRUQsaUNBQVcsR0FBWDtRQUNJLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDL0IsQ0FBQztJQUVLLG1DQUFhLEdBQW5COzs7Ozs0QkFDb0IscUJBQU0sSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQzs0QkFDaEQsU0FBUyxFQUFFLGtCQUFrQjs0QkFDN0IsY0FBYyxFQUFFO2dDQUNaLGdCQUFnQixFQUFFLElBQUksQ0FBQyxVQUFVOzZCQUNwQzt5QkFDSixDQUFDLEVBQUE7O3dCQUxJLE9BQU8sR0FBRyxTQUtkO3dCQUNGLHFCQUFNLE9BQU8sQ0FBQyxPQUFPLEVBQUUsRUFBQTs7d0JBQXZCLFNBQXVCLENBQUM7Ozs7O0tBQzNCOztnQkF2QjhCLGlCQUFpQjs7SUFKdkM7UUFBUixLQUFLLEVBQUU7bURBQWlDO0lBQ2hDO1FBQVIsS0FBSyxFQUFFO21EQUFvQjtJQUZuQixXQUFXO1FBTHZCLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxtQkFBbUI7WUFDN0IsOFRBQWtDOztTQUVyQyxDQUFDO09BQ1csV0FBVyxDQTZCdkI7SUFBRCxrQkFBQztDQUFBLEFBN0JELElBNkJDO1NBN0JZLFdBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkRlc3Ryb3ksIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUG9wb3ZlckNvbnRyb2xsZXIgfSBmcm9tICdAaW9uaWMvYW5ndWxhcic7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBRclBvcG92ZXJDb21wb25lbnQgfSBmcm9tICcuL3FyLXBvcG92ZXIuY29tcG9uZW50JztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdib3h4LXFyLWNvbXBvbmVudCcsXG4gICAgdGVtcGxhdGVVcmw6ICcuL3FyLWNvbXBvbmVudC5odG1sJyxcbiAgICBzdHlsZVVybHM6IFsnLi9xci1jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIFFyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuICAgIEBJbnB1dCgpIGlzTG9hZGluZyQ6IE9ic2VydmFibGU8Ym9vbGVhbj47XG4gICAgQElucHV0KCkgd2Vic2l0ZVVybDogc3RyaW5nO1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByaXZhdGUgcG9wb3ZlckNvbnRyb2xsZXI6IFBvcG92ZXJDb250cm9sbGVyXG4gICAgKSB7IH1cblxuXG4gICAgZGVzdHJveWVkJCA9IG5ldyBTdWJqZWN0PGJvb2xlYW4+KCk7XG5cbiAgICBuZ09uSW5pdCgpIHtcblxuICAgIH1cblxuICAgIG5nT25EZXN0cm95KCkge1xuICAgICAgICB0aGlzLmRlc3Ryb3llZCQubmV4dCh0cnVlKTtcbiAgICAgICAgdGhpcy5kZXN0cm95ZWQkLmNvbXBsZXRlKCk7XG4gICAgfVxuXG4gICAgYXN5bmMgb3BlblFSUG9wb3ZlcigpIHtcbiAgICAgICAgY29uc3QgcG9wb3ZlciA9IGF3YWl0IHRoaXMucG9wb3ZlckNvbnRyb2xsZXIuY3JlYXRlKHtcbiAgICAgICAgICAgIGNvbXBvbmVudDogUXJQb3BvdmVyQ29tcG9uZW50LFxuICAgICAgICAgICAgY29tcG9uZW50UHJvcHM6IHtcbiAgICAgICAgICAgICAgICB3ZWJzaXRlVXJsU3RyaW5nOiB0aGlzLndlYnNpdGVVcmxcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIGF3YWl0IHBvcG92ZXIucHJlc2VudCgpO1xuICAgIH1cbn1cbiJdfQ==