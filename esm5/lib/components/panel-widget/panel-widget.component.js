import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
var PanelWidgetComponent = /** @class */ (function () {
    function PanelWidgetComponent() {
    }
    PanelWidgetComponent.prototype.ngOnInit = function () { };
    __decorate([
        Input()
    ], PanelWidgetComponent.prototype, "title", void 0);
    PanelWidgetComponent = __decorate([
        Component({
            selector: 'boxx-panel-widget',
            template: "<div class=\"boxx-panel-widget\">\n    <ion-item lines=\"none\" [hidden]=\"!title\">\n        <ion-label class=\"widget-title\">{{title}}</ion-label>\n\n        <ion-note slot=\"end\">\n            <ng-content select=\"div[forwardLink]\"></ng-content>\n        </ion-note>\n    </ion-item>\n\n    <ng-content></ng-content>\n</div>",
            styles: [":host .boxx-panel-widget{margin-bottom:16px}:host .widget-title{font-size:larger;font-weight:600}"]
        })
    ], PanelWidgetComponent);
    return PanelWidgetComponent;
}());
export { PanelWidgetComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWwtd2lkZ2V0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3BhbmVsLWNvcmUvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9wYW5lbC13aWRnZXQvcGFuZWwtd2lkZ2V0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFPekQ7SUFHSTtJQUFnQixDQUFDO0lBRWpCLHVDQUFRLEdBQVIsY0FBYSxDQUFDO0lBSkw7UUFBUixLQUFLLEVBQUU7dURBQWU7SUFEZCxvQkFBb0I7UUFMaEMsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLG1CQUFtQjtZQUM3QixzVkFBNEM7O1NBRS9DLENBQUM7T0FDVyxvQkFBb0IsQ0FPaEM7SUFBRCwyQkFBQztDQUFBLEFBUEQsSUFPQztTQVBZLG9CQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2JveHgtcGFuZWwtd2lkZ2V0JyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vcGFuZWwtd2lkZ2V0LmNvbXBvbmVudC5odG1sJyxcbiAgICBzdHlsZVVybHM6IFsnLi9wYW5lbC13aWRnZXQuY29tcG9uZW50LnNjc3MnXSxcbn0pXG5leHBvcnQgY2xhc3MgUGFuZWxXaWRnZXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIEBJbnB1dCgpIHRpdGxlOiBzdHJpbmc7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gICAgbmdPbkluaXQoKSB7IH1cblxufVxuIl19