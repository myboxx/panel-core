import { __decorate } from "tslib";
import { Component } from '@angular/core';
var MainHeaderComponent = /** @class */ (function () {
    function MainHeaderComponent() {
    }
    MainHeaderComponent.prototype.ngOnInit = function () { };
    MainHeaderComponent = __decorate([
        Component({
            selector: 'boxx-main-header',
            template: "<div id=\"panel-header\">\n    <ion-row id=\"panel-header-info\">\n        <ion-col size=\"10\">\n            <ng-content select=\"ion-label\"></ng-content>\n        </ion-col>\n        <ion-col size=\"2\" id=\"header-right-col\">\n            <ng-content select=\"boxx-qr-component\"></ng-content>\n        </ion-col>\n    </ion-row>\n    <ng-content></ng-content>\n</div>",
            styles: ["#panel-header{position:relative;height:160px;border-bottom-left-radius:50% 50%;border-bottom-right-radius:50% 50%;width:120%;margin-top:-1px;margin-left:-10%;padding-left:10%;padding-right:10%;background-color:var(--ion-color-primary,#fff);color:var(--ion-color-primary-contrast,#000);border-left:6px solid var(--ion-color-secondary,#000);border-bottom:6px solid var(--ion-color-secondary,#000);border-right:6px solid var(--ion-color-secondary,#000)}#panel-header-info{font-size:12px;padding:8px}#panel-header-info ion-col{padding:0}#header-right-col{padding:1px;font-size:2.7em;text-align:right}"]
        })
    ], MainHeaderComponent);
    return MainHeaderComponent;
}());
export { MainHeaderComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi1oZWFkZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvcGFuZWwtY29yZS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL21haW4taGVkZXIvbWFpbi1oZWFkZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBT2xEO0lBRUU7SUFBZ0IsQ0FBQztJQUVqQixzQ0FBUSxHQUFSLGNBQVksQ0FBQztJQUpGLG1CQUFtQjtRQUwvQixTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsa0JBQWtCO1lBQzVCLGlZQUEyQzs7U0FFNUMsQ0FBQztPQUNXLG1CQUFtQixDQU0vQjtJQUFELDBCQUFDO0NBQUEsQUFORCxJQU1DO1NBTlksbUJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYm94eC1tYWluLWhlYWRlcicsXG4gIHRlbXBsYXRlVXJsOiAnLi9tYWluLWhlYWRlci5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL21haW4taGVhZGVyLmNvbXBvbmVudC5zY3NzJ10sXG59KVxuZXhwb3J0IGNsYXNzIE1haW5IZWFkZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7fVxuXG59XG4iXX0=