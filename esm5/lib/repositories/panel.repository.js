import { __decorate, __param } from "tslib";
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { AbstractAppConfigService, APP_CONFIG_SERVICE, IHttpBasicResponse } from '@boxx/core';
var PanelRepository = /** @class */ (function () {
    function PanelRepository(appSettings, httpClient) {
        this.appSettings = appSettings;
        this.httpClient = httpClient;
    }
    PanelRepository.prototype.getPanelData = function () {
        // console.log("--- EXECUTING PanelRepository.getPanelData()");
        return this.httpClient.get("" + this.getBaseUrl());
    };
    PanelRepository.prototype.getBaseUrl = function () {
        return this.appSettings.baseUrl() + "/api/" + this.appSettings.instance() + "/v1/home";
    };
    PanelRepository.ctorParameters = function () { return [
        { type: AbstractAppConfigService, decorators: [{ type: Inject, args: [APP_CONFIG_SERVICE,] }] },
        { type: HttpClient }
    ]; };
    PanelRepository = __decorate([
        Injectable(),
        __param(0, Inject(APP_CONFIG_SERVICE))
    ], PanelRepository);
    return PanelRepository;
}());
export { PanelRepository };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWwucmVwb3NpdG9yeS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3BhbmVsLWNvcmUvIiwic291cmNlcyI6WyJsaWIvcmVwb3NpdG9yaWVzL3BhbmVsLnJlcG9zaXRvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNsRCxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsa0JBQWtCLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxZQUFZLENBQUM7QUFLOUY7SUFFSSx5QkFDd0MsV0FBcUMsRUFDakUsVUFBc0I7UUFETSxnQkFBVyxHQUFYLFdBQVcsQ0FBMEI7UUFDakUsZUFBVSxHQUFWLFVBQVUsQ0FBWTtJQUM5QixDQUFDO0lBRUwsc0NBQVksR0FBWjtRQUNJLCtEQUErRDtRQUMvRCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUF3QyxLQUFHLElBQUksQ0FBQyxVQUFVLEVBQUksQ0FBQyxDQUFDO0lBQzlGLENBQUM7SUFFTyxvQ0FBVSxHQUFsQjtRQUNJLE9BQVUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsYUFBUSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRSxhQUFVLENBQUM7SUFDdEYsQ0FBQzs7Z0JBWG9ELHdCQUF3Qix1QkFBeEUsTUFBTSxTQUFDLGtCQUFrQjtnQkFDTixVQUFVOztJQUp6QixlQUFlO1FBRDNCLFVBQVUsRUFBRTtRQUlKLFdBQUEsTUFBTSxDQUFDLGtCQUFrQixDQUFDLENBQUE7T0FIdEIsZUFBZSxDQWUzQjtJQUFELHNCQUFDO0NBQUEsQUFmRCxJQWVDO1NBZlksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEFic3RyYWN0QXBwQ29uZmlnU2VydmljZSwgQVBQX0NPTkZJR19TRVJWSUNFLCBJSHR0cEJhc2ljUmVzcG9uc2UgfSBmcm9tICdAYm94eC9jb3JlJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IElQYW5lbEFwaVJlc3BvbnNlLCBJUGFuZWxSZXBvc2l0b3J5IH0gZnJvbSAnLi9JUGFuZWwucmVwb3NpdG9yeSc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBQYW5lbFJlcG9zaXRvcnkgaW1wbGVtZW50cyBJUGFuZWxSZXBvc2l0b3J5IHtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBASW5qZWN0KEFQUF9DT05GSUdfU0VSVklDRSkgcHJpdmF0ZSBhcHBTZXR0aW5nczogQWJzdHJhY3RBcHBDb25maWdTZXJ2aWNlLFxuICAgICAgICBwcml2YXRlIGh0dHBDbGllbnQ6IEh0dHBDbGllbnQsXG4gICAgKSB7IH1cblxuICAgIGdldFBhbmVsRGF0YSgpOiBPYnNlcnZhYmxlPElIdHRwQmFzaWNSZXNwb25zZTxJUGFuZWxBcGlSZXNwb25zZT4+IHtcbiAgICAgICAgLy8gY29uc29sZS5sb2coXCItLS0gRVhFQ1VUSU5HIFBhbmVsUmVwb3NpdG9yeS5nZXRQYW5lbERhdGEoKVwiKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8SUh0dHBCYXNpY1Jlc3BvbnNlPElQYW5lbEFwaVJlc3BvbnNlPj4oYCR7dGhpcy5nZXRCYXNlVXJsKCl9YCk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXRCYXNlVXJsKCnCoHtcbiAgICAgICAgcmV0dXJuIGAke3RoaXMuYXBwU2V0dGluZ3MuYmFzZVVybCgpfS9hcGkvJHt0aGlzLmFwcFNldHRpbmdzLmluc3RhbmNlKCl9L3YxL2hvbWVgO1xuICAgIH1cbn1cbiJdfQ==