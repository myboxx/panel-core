import { __decorate, __awaiter, __param } from 'tslib';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Input, Component, InjectionToken, Inject, Injectable, ɵɵdefineInjectable, ɵɵinject, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Platform, PopoverController, LoadingController, AlertController, ActionSheetController, IonicModule } from '@ionic/angular';
import { createEffect, ofType, Actions, EffectsModule } from '@ngrx/effects';
import { createAction, props, createReducer, on, createFeatureSelector, createSelector, Store, StoreModule } from '@ngrx/store';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { Subject, of } from 'rxjs';
import { File } from '@ionic-native/file/ngx';
import { takeUntil, map, catchError, switchMap } from 'rxjs/operators';
import { AbstractAppConfigService, APP_CONFIG_SERVICE } from '@boxx/core';
import { LeadModel } from '@boxx/messages-core';
import { EventModel } from '@boxx/events-core';
import { ContactModel } from '@boxx/contacts-core';

let ContactChartWidgetComponent = class ContactChartWidgetComponent {
    constructor() { }
    ngOnInit() { }
};
__decorate([
    Input()
], ContactChartWidgetComponent.prototype, "stats", void 0);
__decorate([
    Input()
], ContactChartWidgetComponent.prototype, "labels", void 0);
ContactChartWidgetComponent = __decorate([
    Component({
        selector: 'boxx-contact-chart-widget',
        template: "<ion-grid class=\"ion-margin-bottom ion-no-padding\">\n    <ion-row>\n        <ion-col size=\"4\" id=\"contacts-chart-col\">\n            <div id=\"contacts-chart-counter\">\n                <div>\n                    <h3>{{stats.total}}</h3>\n                    {{labels.contacts}}\n                </div>\n            </div>\n\n            <div id=\"contacts-chart-container\">\n                <ng-content></ng-content>\n            </div>\n        </ion-col>\n\n        <ion-col size=\"8\" id=\"contacts-stats-col\">\n            <ion-item>\n                <ion-label>\n                    <p>\n                        <span class=\"contact-bullet bullet1\"></span>\n                        {{labels.not_specified}}\n                    </p>\n                </ion-label>\n                <ion-note slot=\"end\" class=\"ion-no-margin\">{{stats.not_specified}}%</ion-note>\n            </ion-item>\n\n            <ion-item>\n                <ion-label>\n                    <p>\n                        <span class=\"contact-bullet bullet2\"></span>\n                        {{labels.prospect}}\n                    </p>\n                </ion-label>\n                <ion-note slot=\"end\" class=\"ion-no-margin\">{{stats.prospect}}%</ion-note>\n            </ion-item>\n\n            <ion-item>\n                <ion-label>\n                    <p>\n                        <span class=\"contact-bullet bullet3\"></span>\n                        {{labels.client}}\n                    </p>\n                </ion-label>\n                <ion-note slot=\"end\" class=\"ion-no-margin\">{{stats.client}}%</ion-note>\n            </ion-item>\n        </ion-col>\n    </ion-row>\n</ion-grid>",
        styles: ["#contacts-chart-col{padding:0}#contacts-stats-col{padding-left:16px}#contacts-chart-container{position:relative;width:69vw;margin-left:-20vw}#contacts-chart-counter{height:100%;position:absolute;z-index:2;text-align:center;align-items:center;display:-ms-grid;display:grid;width:100%}#contacts-chart-counter h3{margin:0}.contact-bullet{width:10px;height:12px;display:inline-flex;margin-right:4px;border-radius:3px}.contact-bullet.bullet1{background:var(--ion-color-primary,red)}.contact-bullet.bullet2{background:var(--ion-color-secondary,green)}.contact-bullet.bullet3{background:var(--ion-color-tertiary,#00f)}"]
    })
], ContactChartWidgetComponent);

let KpiCellComponent = class KpiCellComponent {
    constructor() { }
    ngOnInit() { }
};
__decorate([
    Input()
], KpiCellComponent.prototype, "className", void 0);
__decorate([
    Input()
], KpiCellComponent.prototype, "iconName", void 0);
__decorate([
    Input()
], KpiCellComponent.prototype, "iconSrc", void 0);
__decorate([
    Input()
], KpiCellComponent.prototype, "label", void 0);
__decorate([
    Input()
], KpiCellComponent.prototype, "value", void 0);
KpiCellComponent = __decorate([
    Component({
        selector: 'boxx-kpi-cell',
        template: "<!-- KPI CELL -->\n<div class=\"kpi {{className}}\">\n    <div class=\"kpi-icon\">\n        <ion-icon name=\"{{iconName}}\" src=\"{{iconSrc}}\" [hidden]=\"!iconName && !iconSrc\"></ion-icon>\n    </div>\n    <div class=\"kpi-label\">\n        {{label}}\n    </div>\n    <div class=\"kpi-counter\">\n        <div class=\"value\">{{value}}</div>\n    </div>\n</div>",
        styles: [":host{display:contents}:host .kpi{text-align:center;min-width:120px;max-width:100%;min-height:125px;max-height:300px;padding:10px;box-shadow:#d3d3d3 0 0 4px;border-radius:29px;background-color:#fff;position:relative;display:-ms-grid;display:grid}:host .kpi ion-icon{font-size:x-large;display:flex;margin:auto}"]
    })
], KpiCellComponent);

let MainHeaderComponent = class MainHeaderComponent {
    constructor() { }
    ngOnInit() { }
};
MainHeaderComponent = __decorate([
    Component({
        selector: 'boxx-main-header',
        template: "<div id=\"panel-header\">\n    <ion-row id=\"panel-header-info\">\n        <ion-col size=\"10\">\n            <ng-content select=\"ion-label\"></ng-content>\n        </ion-col>\n        <ion-col size=\"2\" id=\"header-right-col\">\n            <ng-content select=\"boxx-qr-component\"></ng-content>\n        </ion-col>\n    </ion-row>\n    <ng-content></ng-content>\n</div>",
        styles: ["#panel-header{position:relative;height:160px;border-bottom-left-radius:50% 50%;border-bottom-right-radius:50% 50%;width:120%;margin-top:-1px;margin-left:-10%;padding-left:10%;padding-right:10%;background-color:var(--ion-color-primary,#fff);color:var(--ion-color-primary-contrast,#000);border-left:6px solid var(--ion-color-secondary,#000);border-bottom:6px solid var(--ion-color-secondary,#000);border-right:6px solid var(--ion-color-secondary,#000)}#panel-header-info{font-size:12px;padding:8px}#panel-header-info ion-col{padding:0}#header-right-col{padding:1px;font-size:2.7em;text-align:right}"]
    })
], MainHeaderComponent);

let PanelWidgetComponent = class PanelWidgetComponent {
    constructor() { }
    ngOnInit() { }
};
__decorate([
    Input()
], PanelWidgetComponent.prototype, "title", void 0);
PanelWidgetComponent = __decorate([
    Component({
        selector: 'boxx-panel-widget',
        template: "<div class=\"boxx-panel-widget\">\n    <ion-item lines=\"none\" [hidden]=\"!title\">\n        <ion-label class=\"widget-title\">{{title}}</ion-label>\n\n        <ion-note slot=\"end\">\n            <ng-content select=\"div[forwardLink]\"></ng-content>\n        </ion-note>\n    </ion-item>\n\n    <ng-content></ng-content>\n</div>",
        styles: [":host .boxx-panel-widget{margin-bottom:16px}:host .widget-title{font-size:larger;font-weight:600}"]
    })
], PanelWidgetComponent);

let QrPopoverComponent = class QrPopoverComponent {
    constructor(platform, popoverCtrl, file, loaderCtrl, alertCtrl, actionSheetController, translate) {
        this.platform = platform;
        this.popoverCtrl = popoverCtrl;
        this.file = file;
        this.loaderCtrl = loaderCtrl;
        this.alertCtrl = alertCtrl;
        this.actionSheetController = actionSheetController;
        this.translate = translate;
        this.isCordovaOrCapacitor = false;
        this.elementType = 'url';
        this.destroyed$ = new Subject();
        this.translate.get(['GENERAL', 'QR'])
            .pipe(takeUntil(this.destroyed$))
            .subscribe((t) => this.translations = t);
        this.platform.ready().then(() => {
            this.isCordovaOrCapacitor = this.platform.is('cordova') || this.platform.is('capacitor');
            if (this.platform.is('android')) {
                this.isAndroid = true;
                this.folderpath = this.file.externalRootDirectory;
            }
            else {
                this.isAndroid = false;
                this.folderpath = this.file.documentsDirectory;
            }
        });
    }
    ngOnInit() {
        setTimeout(() => {
            const qrMainContainer = document.getElementsByTagName('ngx-qrcode')[0];
            if (!qrMainContainer) {
                return console.error('No hay contenedor para la imagen QR');
            }
            const qrImgContainer = qrMainContainer.firstElementChild;
            if (!qrImgContainer) {
                return console.error('No hay imagen QR');
            }
            const child = qrImgContainer.firstElementChild;
            if (child) {
                const src = child.getAttribute('src');
                if (src) {
                    const a = document.getElementById('downladableQr'); // Create <a>
                    a.setAttribute('src', src);
                }
                else {
                    console.error('--- No img src found');
                }
            }
            else {
                console.error('--- Element not found');
            }
        }, 900);
    }
    close() {
        this.popoverCtrl.dismiss();
    }
    saveImage(src) {
        return __awaiter(this, void 0, void 0, function* () {
            let option;
            const actionSheet = yield this.actionSheetController.create({
                header: this.translations.GENERAL.ACTION.options,
                buttons: [{
                        text: '500 x 500',
                        handler: () => { option = 500; }
                    },
                    {
                        text: '700 x 700',
                        handler: () => { option = 700; }
                    },
                    {
                        text: '1200 x 1200',
                        handler: () => { option = 1200; }
                    }]
            });
            actionSheet.present();
            yield actionSheet.onDidDismiss();
            if (!option) {
                return;
            }
            const loader = yield this.loaderCtrl.create();
            loader.present();
            const scaledImg = yield this.scaleImage(src.getAttribute('src'), option, option)
                .catch(error => {
                this.alertCtrl.create({
                    header: this.translations.GENERAL.error,
                    message: this.translations.QR.scalingError + '<br>' + JSON.stringify(error),
                    buttons: [{ text: this.translations.GENERAL.ACTION.ok }]
                });
                loader.dismiss();
            });
            if (!scaledImg) {
                return;
            }
            const block = (scaledImg || '').split(';');
            // Get the content type
            const dataType = block[0].split(':')[1];
            // get the real base64 content of the file
            const realData = block[1].split(',')[1];
            // The name of your file, note that you need to know if is .png,.jpeg etc
            const filename = `MY_WEBSITE_${option}x${option}.${dataType.split('/')[1]}`;
            this.saveBase64(this.folderpath, realData, filename, dataType).then((path) => __awaiter(this, void 0, void 0, function* () {
                this.close();
                this.alertCtrl.create({
                    header: this.translations.QR.completed,
                    message: yield this.translate.get('QR.fileDownloadedOk', { filename }).toPromise(),
                    buttons: [{ text: this.translations.GENERAL.ACTION.ok }]
                }).then(a => a.present());
            }), error => {
                console.error('saveBase64: error', error);
                this.alertCtrl.create({
                    header: this.translations.GENERAL.error,
                    message: this.translations.QR.fileDownloadError + '<br>' + JSON.stringify(error),
                    buttons: [{ text: this.translations.GENERAL.ACTION.ok }]
                }).then(a => a.present());
            }).finally(() => this.loaderCtrl.dismiss());
        });
    }
    /**
     * Convert a base64 string in a Blob according to the data and contentType.
     *
     * @param b64Data Pure base64 string without contentType
     * @param contentType the content type of the file i.e (image/jpeg - image/png - text/plain)
     * @param sliceSize SliceSize to process the byteCharacters
     * @see http://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
     * @return Blob
     */
    b64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;
        const byteCharacters = atob(b64Data);
        const byteArrays = [];
        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            const slice = byteCharacters.slice(offset, offset + sliceSize);
            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            const byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        const blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }
    saveBase64(pictureDir, content, name, dataType) {
        return new Promise((resolve, reject) => {
            const blob = this.b64toBlob(content, dataType);
            if (!this.isCordovaOrCapacitor) {
                this.downloadFile(blob, name, dataType);
                return resolve(pictureDir + name);
            }
            this.file.writeFile(pictureDir, name, blob, { replace: true })
                .then(() => resolve(pictureDir + name))
                .catch((err) => {
                console.error('error writing blob. ERROR: ', err);
                reject(err);
            });
        });
    }
    scaleImage(base64Data, width, height) {
        return new Promise((resolve, reject) => {
            const img = new Image();
            img.onload = () => {
                // We create a canvas and get its context.
                const canvas = document.createElement('canvas');
                const ctx = canvas.getContext('2d');
                // We set the dimensions at the wanted size.
                canvas.width = width;
                canvas.height = height;
                // We resize the image with the canvas method drawImage();
                ctx.drawImage(img, 0, 0, width, height);
                resolve(canvas.toDataURL());
            };
            img.onerror = ((e) => {
                reject(e.toString());
            });
            img.src = base64Data;
        });
    }
    // source: https://davidwalsh.name/javascript-download
    downloadFile(data, fileName, type = 'text/plain') {
        // Create an invisible A element
        const a = document.createElement('a');
        a.style.display = 'none';
        document.body.appendChild(a);
        // Set the HREF to a Blob representation of the data to be downloaded
        a.href = window.URL.createObjectURL(new Blob([data], { type }));
        // Use download attribute to set set desired file name
        a.setAttribute('download', fileName);
        // Trigger the download by simulating click
        a.click();
        // Cleanup
        window.URL.revokeObjectURL(a.href);
        document.body.removeChild(a);
    }
};
QrPopoverComponent.ctorParameters = () => [
    { type: Platform },
    { type: PopoverController },
    { type: File },
    { type: LoadingController },
    { type: AlertController },
    { type: ActionSheetController },
    { type: TranslateService }
];
__decorate([
    Input()
], QrPopoverComponent.prototype, "websiteUrlString", void 0);
QrPopoverComponent = __decorate([
    Component({
        selector: 'boxx-qr-popover',
        template: "<ion-content class=\"ion-padding-bottom\">\n    <div class=\"qr-toolbar\">\n        <ion-button fill=\"clear\" (click)=\"close()\">\n            <ion-icon name=\"close-circle-outline\"></ion-icon>\n        </ion-button>\n    </div>\n    <div class=\"ion-text-center\" *ngIf=\"websiteUrlString\">\n        <ngx-qrcode [elementType]=\"elementType\" [value]=\"websiteUrlString\" cssClass=\"qr-container\"\n            errorCorrectionLevel=\"L\">\n        </ngx-qrcode>\n\n        <ion-button color=\"primary\" id=\"downladableQr\" (click)=\"saveImage($event.target)\" translate>\n            GENERAL.ACTION.download\n        </ion-button>\n\n    </div>\n</ion-content>",
        styles: [":host ion-content{--background:white}:host .qr-toolbar{text-align:right}"]
    })
], QrPopoverComponent);

let QrComponent = class QrComponent {
    constructor(popoverController) {
        this.popoverController = popoverController;
        this.destroyed$ = new Subject();
    }
    ngOnInit() {
    }
    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }
    openQRPopover() {
        return __awaiter(this, void 0, void 0, function* () {
            const popover = yield this.popoverController.create({
                component: QrPopoverComponent,
                componentProps: {
                    websiteUrlString: this.websiteUrl
                }
            });
            yield popover.present();
        });
    }
};
QrComponent.ctorParameters = () => [
    { type: PopoverController }
];
__decorate([
    Input()
], QrComponent.prototype, "isLoading$", void 0);
__decorate([
    Input()
], QrComponent.prototype, "websiteUrl", void 0);
QrComponent = __decorate([
    Component({
        selector: 'boxx-qr-component',
        template: "<div id=\"qr-spinner-container\" *ngIf=\"(isLoading$ | async) === true\">\n    <ion-spinner  id=\"qr-spinner\"></ion-spinner>\n</div>\n<div *ngIf=\"websiteUrl && (isLoading$ | async) === false\">\n    <ion-icon src=\"assets/icon/qr.svg\" slot=\"end\" (click)=\"openQRPopover()\">\n    </ion-icon>\n</div>\n",
        styles: ["#qr-spinner-container{width:100%;text-align:center}#qr-spinner-container ion-spinner{color:var(--ion-color-primary-contrast,#fff)}ion-icon{border-radius:5px;padding:5px;background:var(--ion-color-primary-contrast,#fff)}"]
    })
], QrComponent);

const PANEL_REPOSITORY = new InjectionToken('panelRepository');

let PanelRepository = class PanelRepository {
    constructor(appSettings, httpClient) {
        this.appSettings = appSettings;
        this.httpClient = httpClient;
    }
    getPanelData() {
        // console.log("--- EXECUTING PanelRepository.getPanelData()");
        return this.httpClient.get(`${this.getBaseUrl()}`);
    }
    getBaseUrl() {
        return `${this.appSettings.baseUrl()}/api/${this.appSettings.instance()}/v1/home`;
    }
};
PanelRepository.ctorParameters = () => [
    { type: AbstractAppConfigService, decorators: [{ type: Inject, args: [APP_CONFIG_SERVICE,] }] },
    { type: HttpClient }
];
PanelRepository = __decorate([
    Injectable(),
    __param(0, Inject(APP_CONFIG_SERVICE))
], PanelRepository);

const PANEL_SERVICE = new InjectionToken('panelService');

class PanelPageModel {
    constructor(data) {
        this.websiteData = data.websiteData;
        this.contactsData = data.contactsData;
        this.messagesData = data.messagesData;
        this.eventsData = data.eventsData;
    }
    static fromApiResponse(data) {
        const lastContacts = data.contacts.recent_contacts.map(c => ContactModel.fromDataResponse(c));
        const lastMessages = data.form_messages.last_received.map(m => LeadModel.fromApiResponse(m));
        const upcomingEvents = data.schedule.upcoming_events.map(e => EventModel.fromDataResponse(e))
            .sort((a, b) => a.datetimeFrom.localeCompare(b.datetimeFrom));
        return new PanelPageModel({
            websiteData: { pageViews: +data.analytics.page_views },
            contactsData: {
                lastContacts,
                stats: {
                    total: +data.contacts.stats.total,
                    client: +data.contacts.stats.clients,
                    prospect: +data.contacts.stats.prospects,
                    notSpecified: +data.contacts.stats.not_specified
                }
            },
            messagesData: {
                lastMessages,
                stats: {
                    total: +data.form_messages.stats.total,
                    read: +data.form_messages.stats.read,
                    unread: +data.form_messages.stats.unread,
                }
            },
            eventsData: {
                upcomingEvents,
                stats: { total: +data.schedule.stats.total }
            }
        });
    }
    static empty() {
        return new PanelPageModel({
            websiteData: { pageViews: 0 },
            contactsData: {
                lastContacts: [],
                stats: {
                    total: 0,
                    client: 0,
                    prospect: 0,
                    notSpecified: 0
                }
            },
            messagesData: {
                lastMessages: [],
                stats: {
                    total: 0,
                    read: 0,
                    unread: 0,
                }
            },
            eventsData: {
                upcomingEvents: [],
                stats: { total: 0 }
            }
        });
    }
}

let PanelService = class PanelService {
    constructor(repository) {
        this.repository = repository;
    }
    getPanelData() {
        return this.repository.getPanelData().pipe(map((response) => {
            return PanelPageModel.fromApiResponse(response.data);
        }), catchError(error => {
            throw error;
        }));
    }
};
PanelService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [PANEL_REPOSITORY,] }] }
];
PanelService.ɵprov = ɵɵdefineInjectable({ factory: function PanelService_Factory() { return new PanelService(ɵɵinject(PANEL_REPOSITORY)); }, token: PanelService, providedIn: "root" });
PanelService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(0, Inject(PANEL_REPOSITORY))
], PanelService);

var PanelActionTypes;
(function (PanelActionTypes) {
    PanelActionTypes["LoadPanelBegin"] = "[PANEL] Load Panel Begin";
    PanelActionTypes["LoadPanelSuccess"] = "[PANEL] Load Panel Success";
    PanelActionTypes["LoadPanelFail"] = "[PANEL] Load Panel Fail";
})(PanelActionTypes || (PanelActionTypes = {}));
const LoadPanelBeginAction = createAction(PanelActionTypes.LoadPanelBegin);
const LoadPanelSuccessAction = createAction(PanelActionTypes.LoadPanelSuccess, props());
const LoadPanelFailAction = createAction(PanelActionTypes.LoadPanelFail, props());

let PanelEffects = class PanelEffects {
    constructor(actions$, service) {
        this.actions$ = actions$;
        this.service = service;
        this.loadPanel$ = createEffect(() => this.actions$.pipe(ofType(PanelActionTypes.LoadPanelBegin), switchMap(() => {
            return this.service.getPanelData().pipe(map(panel => LoadPanelSuccessAction({ payload: panel })), catchError(error => {
                console.error('Couldn\'t load main panel', error);
                return of(LoadPanelFailAction({ errors: error }));
            }));
        })));
    }
};
PanelEffects.ctorParameters = () => [
    { type: Actions },
    { type: undefined, decorators: [{ type: Inject, args: [PANEL_SERVICE,] }] }
];
PanelEffects = __decorate([
    Injectable(),
    __param(1, Inject(PANEL_SERVICE))
], PanelEffects);

const ɵ0 = PanelPageModel.empty();
const initialState = {
    isLoading: false,
    hasBeenFetched: false,
    data: ɵ0,
    error: null,
    success: null
};
const ɵ1 = (state) => (Object.assign(Object.assign({}, state), { error: null, success: null, isLoading: true })), ɵ2 = (state, action) => (Object.assign(Object.assign({}, state), { hasBeenFetched: true, data: action.payload, isLoading: false, error: null, success: { after: getSuccessActionType(action.type) } })), ɵ3 = (state, action) => (Object.assign(Object.assign({}, state), { isLoading: false, error: { after: getErrorActionType(action.type), error: action.errors } }));
const reducer = createReducer(initialState, on(LoadPanelBeginAction, ɵ1), on(LoadPanelSuccessAction, ɵ2), on(LoadPanelFailAction, ɵ3));
function getErrorActionType(type) {
    let action = 'UNKNOWN';
    switch (type) {
        case PanelActionTypes.LoadPanelFail:
            action = 'GET';
            break;
    }
    return action;
}
function getSuccessActionType(type) {
    let action = 'UNKNOWN';
    switch (type) {
        case PanelActionTypes.LoadPanelSuccess:
            action = 'GET';
            break;
    }
    return action;
}
function panelReducer(state, action) {
    return reducer(state, action);
}

const getPanelState = createFeatureSelector('panel');
const ɵ0$1 = state => state;
const getPanelPageState = createSelector(getPanelState, ɵ0$1);
const stateGetIsLoading = (state) => state.isLoading;
const ɵ1$1 = stateGetIsLoading;
const stateGetData = (state) => state.data;
const getIsLoading = createSelector(getPanelPageState, stateGetIsLoading);
const ɵ2$1 = state => state.error;
const getError = createSelector(getPanelPageState, ɵ2$1);
const ɵ3$1 = state => state.success;
const getSuccess = createSelector(getPanelPageState, ɵ3$1);
const getData = createSelector(getPanelPageState, stateGetData);
const ɵ4 = state => state.hasBeenFetched;
const hasBeenFetched = createSelector(getPanelPageState, ɵ4);

let PanelStore = class PanelStore {
    constructor(store) {
        this.store = store;
    }
    get Loading$() { return this.store.select(getIsLoading); }
    get Error$() { return this.store.select(getError); }
    get Success$() { return this.store.select(getSuccess); }
    loadPanel() {
        return this.store.dispatch(LoadPanelBeginAction());
    }
    get Panel$() {
        return this.store.select(getData);
    }
    get HasBeenFetched$() {
        return this.store.select(hasBeenFetched);
    }
};
PanelStore.ctorParameters = () => [
    { type: Store }
];
PanelStore = __decorate([
    Injectable()
], PanelStore);

var PanelCoreModule_1;
let PanelCoreModule = PanelCoreModule_1 = class PanelCoreModule {
    static forRoot(config) {
        return {
            ngModule: PanelCoreModule_1,
            providers: [
                { provide: PANEL_SERVICE, useClass: PanelService },
                { provide: PANEL_REPOSITORY, useClass: PanelRepository },
                ...config.providers,
                PanelStore
            ]
        };
    }
};
PanelCoreModule = PanelCoreModule_1 = __decorate([
    NgModule({
        declarations: [
            ContactChartWidgetComponent,
            KpiCellComponent,
            MainHeaderComponent,
            PanelWidgetComponent,
            QrComponent,
            QrPopoverComponent,
        ],
        imports: [
            HttpClientModule,
            StoreModule.forFeature('panel', panelReducer),
            EffectsModule.forFeature([PanelEffects]),
            TranslateModule.forChild(),
            CommonModule,
            FormsModule,
            IonicModule,
            NgxQRCodeModule
        ],
        exports: [
            ContactChartWidgetComponent,
            KpiCellComponent,
            MainHeaderComponent,
            PanelWidgetComponent,
            QrComponent,
            QrPopoverComponent,
        ]
    })
], PanelCoreModule);

/*
 * Public API Surface of panel-core
 */

/**
 * Generated bundle index. Do not edit.
 */

export { LoadPanelBeginAction, LoadPanelFailAction, LoadPanelSuccessAction, PANEL_REPOSITORY, PANEL_SERVICE, PanelActionTypes, PanelCoreModule, PanelEffects, PanelPageModel, PanelRepository, PanelService, PanelStore, getData, getError, getIsLoading, getPanelPageState, getPanelState, getSuccess, hasBeenFetched, initialState, panelReducer, stateGetData, ɵ0$1 as ɵ0, ɵ1$1 as ɵ1, ɵ2$1 as ɵ2, ɵ3$1 as ɵ3, ɵ4, ContactChartWidgetComponent as ɵa, KpiCellComponent as ɵb, MainHeaderComponent as ɵc, PanelWidgetComponent as ɵd, QrComponent as ɵe, QrPopoverComponent as ɵf };
//# sourceMappingURL=boxx-panel-core.js.map
