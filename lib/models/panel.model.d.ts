import { IPanelApiResponse } from '../repositories/IPanel.repository';
import { LeadModel } from '@boxx/messages-core';
import { EventModel } from '@boxx/events-core';
import { ContactModel } from '@boxx/contacts-core';
export declare class PanelPageModel implements IPanelProps {
    constructor(data: IPanelProps);
    websiteData: {
        pageViews: number;
    };
    contactsData: {
        lastContacts: ContactModel[];
        stats: {
            total: number;
            client: number;
            prospect: number;
            notSpecified: number;
        };
    };
    messagesData: {
        lastMessages: LeadModel[];
        stats: {
            total: number;
            read: number;
            unread: number;
        };
    };
    eventsData: {
        upcomingEvents: EventModel[];
        stats: {
            total: number;
        };
    };
    static fromApiResponse(data: IPanelApiResponse): PanelPageModel;
    static empty(): PanelPageModel;
}
export interface IPanelProps {
    websiteData: {
        pageViews: number;
    };
    contactsData: {
        lastContacts: Array<ContactModel>;
        stats: {
            total: number;
            client: number;
            prospect: number;
            notSpecified: number;
        };
    };
    messagesData: {
        lastMessages: Array<LeadModel>;
        stats: {
            total: number;
            read: number;
            unread: number;
        };
    };
    eventsData: {
        upcomingEvents: Array<EventModel>;
        stats: {
            total: number;
        };
    };
}
