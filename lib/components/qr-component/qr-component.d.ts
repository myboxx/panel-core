import { OnDestroy, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { Observable, Subject } from 'rxjs';
export declare class QrComponent implements OnInit, OnDestroy {
    private popoverController;
    isLoading$: Observable<boolean>;
    websiteUrl: string;
    constructor(popoverController: PopoverController);
    destroyed$: Subject<boolean>;
    ngOnInit(): void;
    ngOnDestroy(): void;
    openQRPopover(): Promise<void>;
}
