import { OnInit } from '@angular/core';
import { File } from '@ionic-native/file/ngx';
import { ActionSheetController, AlertController, LoadingController, Platform, PopoverController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
export declare class QrPopoverComponent implements OnInit {
    platform: Platform;
    private popoverCtrl;
    private file;
    private loaderCtrl;
    private alertCtrl;
    private actionSheetController;
    private translate;
    websiteUrlString: string;
    constructor(platform: Platform, popoverCtrl: PopoverController, file: File, loaderCtrl: LoadingController, alertCtrl: AlertController, actionSheetController: ActionSheetController, translate: TranslateService);
    isCordovaOrCapacitor: boolean;
    elementType: string;
    folderpath: string;
    isAndroid: boolean;
    translations: any;
    destroyed$: Subject<boolean>;
    ngOnInit(): void;
    close(): void;
    saveImage(src: HTMLElement): Promise<void>;
    /**
     * Convert a base64 string in a Blob according to the data and contentType.
     *
     * @param b64Data Pure base64 string without contentType
     * @param contentType the content type of the file i.e (image/jpeg - image/png - text/plain)
     * @param sliceSize SliceSize to process the byteCharacters
     * @see http://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
     * @return Blob
     */
    b64toBlob(b64Data: any, contentType: any, sliceSize?: any): Blob;
    saveBase64(pictureDir: string, content: string, name: string, dataType: string): Promise<string>;
    scaleImage(base64Data: string, width: number, height: number): Promise<string>;
    downloadFile(data: any, fileName: any, type?: string): void;
}
