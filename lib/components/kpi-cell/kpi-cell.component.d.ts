import { OnInit } from '@angular/core';
export declare class KpiCellComponent implements OnInit {
    className: string;
    iconName: string;
    iconSrc: string;
    label: string;
    value: number;
    constructor();
    ngOnInit(): void;
}
