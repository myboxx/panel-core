import { OnInit } from '@angular/core';
export declare class ContactChartWidgetComponent implements OnInit {
    stats: {
        total: number;
        client: number;
        prospect: number;
        not_specified: number;
    };
    labels: {
        contacts: string;
        not_specified: string;
        prospect: string;
        client: string;
    };
    constructor();
    ngOnInit(): void;
}
