import { Actions } from '@ngrx/effects';
import { IPanelService } from '../services/IPanel.service';
import * as PanelActions from './panel.actions';
export declare class PanelEffects {
    private actions$;
    private service;
    loadPanel$: import("rxjs").Observable<({
        payload: any;
    } & import("@ngrx/store/src/models").TypedAction<PanelActions.PanelActionTypes.LoadPanelSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<PanelActions.PanelActionTypes.LoadPanelFail>)> & import("@ngrx/effects").CreateEffectMetadata;
    constructor(actions$: Actions, service: IPanelService);
}
