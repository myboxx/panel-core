import { Store } from '@ngrx/store';
import * as fromReducer from './panel.reducer';
export declare class PanelStore {
    private store;
    constructor(store: Store<fromReducer.PanelState>);
    get Loading$(): import("rxjs").Observable<boolean>;
    get Error$(): import("rxjs").Observable<import("../core/IStateErrorSuccess").IPanelStateError>;
    get Success$(): import("rxjs").Observable<import("../core/IStateErrorSuccess").IPanelStateSuccess>;
    loadPanel(): void;
    get Panel$(): import("rxjs").Observable<import("../models/panel.model").PanelPageModel>;
    get HasBeenFetched$(): import("rxjs").Observable<boolean>;
}
