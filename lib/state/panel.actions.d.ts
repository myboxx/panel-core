export declare enum PanelActionTypes {
    LoadPanelBegin = "[PANEL] Load Panel Begin",
    LoadPanelSuccess = "[PANEL] Load Panel Success",
    LoadPanelFail = "[PANEL] Load Panel Fail"
}
export declare const LoadPanelBeginAction: import("@ngrx/store").ActionCreator<PanelActionTypes.LoadPanelBegin, () => import("@ngrx/store/src/models").TypedAction<PanelActionTypes.LoadPanelBegin>>;
export declare const LoadPanelSuccessAction: import("@ngrx/store").ActionCreator<PanelActionTypes.LoadPanelSuccess, (props: {
    payload: any;
}) => {
    payload: any;
} & import("@ngrx/store/src/models").TypedAction<PanelActionTypes.LoadPanelSuccess>>;
export declare const LoadPanelFailAction: import("@ngrx/store").ActionCreator<PanelActionTypes.LoadPanelFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<PanelActionTypes.LoadPanelFail>>;
