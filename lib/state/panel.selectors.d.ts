import * as fromPanelReducer from './panel.reducer';
export declare const getPanelState: import("@ngrx/store").MemoizedSelector<object, fromPanelReducer.PanelState, import("@ngrx/store").DefaultProjectorFn<fromPanelReducer.PanelState>>;
export declare const getPanelPageState: import("@ngrx/store").MemoizedSelector<object, fromPanelReducer.PanelState, import("@ngrx/store").DefaultProjectorFn<fromPanelReducer.PanelState>>;
export declare const stateGetData: (state: fromPanelReducer.PanelState) => import("../models/panel.model").PanelPageModel;
export declare const getIsLoading: import("@ngrx/store").MemoizedSelector<object, boolean, import("@ngrx/store").DefaultProjectorFn<boolean>>;
export declare const getError: import("@ngrx/store").MemoizedSelector<object, import("../core/IStateErrorSuccess").IPanelStateError, import("@ngrx/store").DefaultProjectorFn<import("../core/IStateErrorSuccess").IPanelStateError>>;
export declare const getSuccess: import("@ngrx/store").MemoizedSelector<object, import("../core/IStateErrorSuccess").IPanelStateSuccess, import("@ngrx/store").DefaultProjectorFn<import("../core/IStateErrorSuccess").IPanelStateSuccess>>;
export declare const getData: import("@ngrx/store").MemoizedSelector<object, import("../models/panel.model").PanelPageModel, import("@ngrx/store").DefaultProjectorFn<import("../models/panel.model").PanelPageModel>>;
export declare const hasBeenFetched: import("@ngrx/store").MemoizedSelector<object, boolean, import("@ngrx/store").DefaultProjectorFn<boolean>>;
