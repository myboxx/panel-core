import { Action } from '@ngrx/store';
import { IPanelStateError, IPanelStateSuccess } from '../core/IStateErrorSuccess';
import { PanelPageModel } from '../models/panel.model';
export interface PanelState {
    isLoading: boolean;
    hasBeenFetched: boolean;
    data: PanelPageModel;
    error: IPanelStateError;
    success: IPanelStateSuccess;
}
export declare const initialState: PanelState;
export declare function panelReducer(state: PanelState | undefined, action: Action): PanelState;
