import { Observable } from 'rxjs';
import { PanelPageModel } from '../models/panel.model';
import { IPanelRepository } from '../repositories/IPanel.repository';
import { IPanelService } from './IPanel.service';
export declare class PanelService implements IPanelService {
    private repository;
    constructor(repository: IPanelRepository);
    getPanelData(): Observable<PanelPageModel>;
}
