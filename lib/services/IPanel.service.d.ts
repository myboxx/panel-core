import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { PanelPageModel } from '../models/panel.model';
export interface IPanelService {
    getPanelData(): Observable<PanelPageModel>;
}
export declare const PANEL_SERVICE: InjectionToken<IPanelService>;
