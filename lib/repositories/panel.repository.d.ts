import { HttpClient } from '@angular/common/http';
import { AbstractAppConfigService, IHttpBasicResponse } from '@boxx/core';
import { Observable } from 'rxjs';
import { IPanelApiResponse, IPanelRepository } from './IPanel.repository';
export declare class PanelRepository implements IPanelRepository {
    private appSettings;
    private httpClient;
    constructor(appSettings: AbstractAppConfigService, httpClient: HttpClient);
    getPanelData(): Observable<IHttpBasicResponse<IPanelApiResponse>>;
    private getBaseUrl;
}
