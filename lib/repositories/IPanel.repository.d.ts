import { InjectionToken } from '@angular/core';
import { IContactApiProps } from '@boxx/contacts-core';
import { IHttpBasicResponse } from '@boxx/core';
import { IEventsApiProps } from '@boxx/events-core';
import { ILeadApi } from '@boxx/messages-core';
import { Observable } from 'rxjs';
export interface IPanelRepository {
    getPanelData(): Observable<IHttpBasicResponse<IPanelApiResponse>>;
}
export declare const PANEL_REPOSITORY: InjectionToken<IPanelRepository>;
export interface IPanelApiResponse {
    analytics: {
        page_views: string;
    };
    contacts: {
        recent_contacts: Array<IContactApiProps>;
        stats: {
            clients: string;
            not_specified: string;
            prospects: string;
            total: string;
        };
    };
    form_messages: {
        last_received: Array<ILeadApi>;
        stats: {
            read: string;
            total: string;
            unread: string;
        };
    };
    schedule: {
        stats: {
            total: string;
        };
        upcoming_events: Array<IEventsApiProps>;
    };
}
