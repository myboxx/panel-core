import { ModuleWithProviders, Provider } from '@angular/core';
interface ModuleOptionsInterface {
    providers: Provider[];
}
export declare class PanelCoreModule {
    static forRoot(config: ModuleOptionsInterface): ModuleWithProviders<PanelCoreModule>;
}
export {};
