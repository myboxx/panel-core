var PanelCoreModule_1;
import { __decorate } from "tslib";
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { TranslateModule } from '@ngx-translate/core';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { ContactChartWidgetComponent } from './components/contact-chart-widget/contact-chart-widget.component';
import { KpiCellComponent } from './components/kpi-cell/kpi-cell.component';
import { MainHeaderComponent } from './components/main-heder/main-header.component';
import { PanelWidgetComponent } from './components/panel-widget/panel-widget.component';
import { QrComponent } from './components/qr-component/qr-component';
import { QrPopoverComponent } from './components/qr-component/qr-popover.component';
import { PANEL_REPOSITORY } from './repositories/IPanel.repository';
import { PanelRepository } from './repositories/panel.repository';
import { PANEL_SERVICE } from './services/IPanel.service';
import { PanelService } from './services/Panel.service';
import { PanelEffects } from './state/panel.effects';
import { panelReducer } from './state/panel.reducer';
import { PanelStore } from './state/panel.store';
let PanelCoreModule = PanelCoreModule_1 = class PanelCoreModule {
    static forRoot(config) {
        return {
            ngModule: PanelCoreModule_1,
            providers: [
                { provide: PANEL_SERVICE, useClass: PanelService },
                { provide: PANEL_REPOSITORY, useClass: PanelRepository },
                ...config.providers,
                PanelStore
            ]
        };
    }
};
PanelCoreModule = PanelCoreModule_1 = __decorate([
    NgModule({
        declarations: [
            ContactChartWidgetComponent,
            KpiCellComponent,
            MainHeaderComponent,
            PanelWidgetComponent,
            QrComponent,
            QrPopoverComponent,
        ],
        imports: [
            HttpClientModule,
            StoreModule.forFeature('panel', panelReducer),
            EffectsModule.forFeature([PanelEffects]),
            TranslateModule.forChild(),
            CommonModule,
            FormsModule,
            IonicModule,
            NgxQRCodeModule
        ],
        exports: [
            ContactChartWidgetComponent,
            KpiCellComponent,
            MainHeaderComponent,
            PanelWidgetComponent,
            QrComponent,
            QrPopoverComponent,
        ]
    })
], PanelCoreModule);
export { PanelCoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWwtY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9wYW5lbC1jb3JlLyIsInNvdXJjZXMiOlsibGliL3BhbmVsLWNvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3hELE9BQU8sRUFBdUIsUUFBUSxFQUFZLE1BQU0sZUFBZSxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5QyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBQzFDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDNUQsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0sa0VBQWtFLENBQUM7QUFDL0csT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDNUUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDcEYsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFDeEYsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBQ3BGLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUNsRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDMUQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUNyRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDckQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBa0NqRCxJQUFhLGVBQWUsdUJBQTVCLE1BQWEsZUFBZTtJQUN4QixNQUFNLENBQUMsT0FBTyxDQUFDLE1BQThCO1FBQ3pDLE9BQU87WUFDSCxRQUFRLEVBQUUsaUJBQWU7WUFDekIsU0FBUyxFQUFFO2dCQUNQLEVBQUUsT0FBTyxFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFO2dCQUNsRCxFQUFFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxRQUFRLEVBQUUsZUFBZSxFQUFFO2dCQUN4RCxHQUFHLE1BQU0sQ0FBQyxTQUFTO2dCQUNuQixVQUFVO2FBQ2I7U0FDSixDQUFDO0lBQ04sQ0FBQztDQUNKLENBQUE7QUFaWSxlQUFlO0lBNUIzQixRQUFRLENBQUM7UUFDUixZQUFZLEVBQUU7WUFDWiwyQkFBMkI7WUFDM0IsZ0JBQWdCO1lBQ2hCLG1CQUFtQjtZQUNuQixvQkFBb0I7WUFDcEIsV0FBVztZQUNYLGtCQUFrQjtTQUNuQjtRQUNELE9BQU8sRUFBRTtZQUNQLGdCQUFnQjtZQUNoQixXQUFXLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxZQUFZLENBQUM7WUFDN0MsYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3hDLGVBQWUsQ0FBQyxRQUFRLEVBQUU7WUFDMUIsWUFBWTtZQUNaLFdBQVc7WUFDWCxXQUFXO1lBQ1gsZUFBZTtTQUNoQjtRQUNELE9BQU8sRUFBRTtZQUNQLDJCQUEyQjtZQUMzQixnQkFBZ0I7WUFDaEIsbUJBQW1CO1lBQ25CLG9CQUFvQjtZQUNwQixXQUFXO1lBQ1gsa0JBQWtCO1NBQ25CO0tBQ0YsQ0FBQztHQUNXLGVBQWUsQ0FZM0I7U0FaWSxlQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBNb2R1bGVXaXRoUHJvdmlkZXJzLCBOZ01vZHVsZSwgUHJvdmlkZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgSW9uaWNNb2R1bGUgfSBmcm9tICdAaW9uaWMvYW5ndWxhcic7XG5pbXBvcnQgeyBFZmZlY3RzTW9kdWxlIH0gZnJvbSAnQG5ncngvZWZmZWN0cyc7XG5pbXBvcnQgeyBTdG9yZU1vZHVsZSB9IGZyb20gJ0BuZ3J4L3N0b3JlJztcbmltcG9ydCB7IFRyYW5zbGF0ZU1vZHVsZSB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xuaW1wb3J0IHsgTmd4UVJDb2RlTW9kdWxlIH0gZnJvbSAnQHRlY2hpZWRpYXJpZXMvbmd4LXFyY29kZSc7XG5pbXBvcnQgeyBDb250YWN0Q2hhcnRXaWRnZXRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY29udGFjdC1jaGFydC13aWRnZXQvY29udGFjdC1jaGFydC13aWRnZXQuY29tcG9uZW50JztcbmltcG9ydCB7IEtwaUNlbGxDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMva3BpLWNlbGwva3BpLWNlbGwuY29tcG9uZW50JztcbmltcG9ydCB7IE1haW5IZWFkZXJDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvbWFpbi1oZWRlci9tYWluLWhlYWRlci5jb21wb25lbnQnO1xuaW1wb3J0IHsgUGFuZWxXaWRnZXRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvcGFuZWwtd2lkZ2V0L3BhbmVsLXdpZGdldC5jb21wb25lbnQnO1xuaW1wb3J0IHsgUXJDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvcXItY29tcG9uZW50L3FyLWNvbXBvbmVudCc7XG5pbXBvcnQgeyBRclBvcG92ZXJDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvcXItY29tcG9uZW50L3FyLXBvcG92ZXIuY29tcG9uZW50JztcbmltcG9ydCB7IFBBTkVMX1JFUE9TSVRPUlkgfSBmcm9tICcuL3JlcG9zaXRvcmllcy9JUGFuZWwucmVwb3NpdG9yeSc7XG5pbXBvcnQgeyBQYW5lbFJlcG9zaXRvcnkgfSBmcm9tICcuL3JlcG9zaXRvcmllcy9wYW5lbC5yZXBvc2l0b3J5JztcbmltcG9ydCB7IFBBTkVMX1NFUlZJQ0UgfSBmcm9tICcuL3NlcnZpY2VzL0lQYW5lbC5zZXJ2aWNlJztcbmltcG9ydCB7IFBhbmVsU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvUGFuZWwuc2VydmljZSc7XG5pbXBvcnQgeyBQYW5lbEVmZmVjdHMgfSBmcm9tICcuL3N0YXRlL3BhbmVsLmVmZmVjdHMnO1xuaW1wb3J0IHsgcGFuZWxSZWR1Y2VyIH0gZnJvbSAnLi9zdGF0ZS9wYW5lbC5yZWR1Y2VyJztcbmltcG9ydCB7IFBhbmVsU3RvcmUgfSBmcm9tICcuL3N0YXRlL3BhbmVsLnN0b3JlJztcblxuaW50ZXJmYWNlIE1vZHVsZU9wdGlvbnNJbnRlcmZhY2Uge1xuICAgIHByb3ZpZGVyczogUHJvdmlkZXJbXTtcbn1cblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgQ29udGFjdENoYXJ0V2lkZ2V0Q29tcG9uZW50LFxuICAgIEtwaUNlbGxDb21wb25lbnQsXG4gICAgTWFpbkhlYWRlckNvbXBvbmVudCxcbiAgICBQYW5lbFdpZGdldENvbXBvbmVudCxcbiAgICBRckNvbXBvbmVudCxcbiAgICBRclBvcG92ZXJDb21wb25lbnQsXG4gIF0sXG4gIGltcG9ydHM6IFtcbiAgICBIdHRwQ2xpZW50TW9kdWxlLFxuICAgIFN0b3JlTW9kdWxlLmZvckZlYXR1cmUoJ3BhbmVsJywgcGFuZWxSZWR1Y2VyKSxcbiAgICBFZmZlY3RzTW9kdWxlLmZvckZlYXR1cmUoW1BhbmVsRWZmZWN0c10pLFxuICAgIFRyYW5zbGF0ZU1vZHVsZS5mb3JDaGlsZCgpLFxuICAgIENvbW1vbk1vZHVsZSxcbiAgICBGb3Jtc01vZHVsZSxcbiAgICBJb25pY01vZHVsZSxcbiAgICBOZ3hRUkNvZGVNb2R1bGVcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIENvbnRhY3RDaGFydFdpZGdldENvbXBvbmVudCxcbiAgICBLcGlDZWxsQ29tcG9uZW50LFxuICAgIE1haW5IZWFkZXJDb21wb25lbnQsXG4gICAgUGFuZWxXaWRnZXRDb21wb25lbnQsXG4gICAgUXJDb21wb25lbnQsXG4gICAgUXJQb3BvdmVyQ29tcG9uZW50LFxuICBdXG59KVxuZXhwb3J0IGNsYXNzIFBhbmVsQ29yZU1vZHVsZSB7XG4gICAgc3RhdGljIGZvclJvb3QoY29uZmlnOiBNb2R1bGVPcHRpb25zSW50ZXJmYWNlKTogTW9kdWxlV2l0aFByb3ZpZGVyczxQYW5lbENvcmVNb2R1bGU+IHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIG5nTW9kdWxlOiBQYW5lbENvcmVNb2R1bGUsXG4gICAgICAgICAgICBwcm92aWRlcnM6IFtcbiAgICAgICAgICAgICAgICB7IHByb3ZpZGU6IFBBTkVMX1NFUlZJQ0UsIHVzZUNsYXNzOiBQYW5lbFNlcnZpY2UgfSxcbiAgICAgICAgICAgICAgICB7IHByb3ZpZGU6IFBBTkVMX1JFUE9TSVRPUlksIHVzZUNsYXNzOiBQYW5lbFJlcG9zaXRvcnkgfSxcbiAgICAgICAgICAgICAgICAuLi5jb25maWcucHJvdmlkZXJzLFxuICAgICAgICAgICAgICAgIFBhbmVsU3RvcmVcbiAgICAgICAgICAgIF1cbiAgICAgICAgfTtcbiAgICB9XG59XG4iXX0=