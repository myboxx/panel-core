import { __decorate, __param } from "tslib";
import { Inject, Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { PanelPageModel } from '../models/panel.model';
import { PANEL_REPOSITORY } from '../repositories/IPanel.repository';
import * as i0 from "@angular/core";
import * as i1 from "../repositories/IPanel.repository";
let PanelService = class PanelService {
    constructor(repository) {
        this.repository = repository;
    }
    getPanelData() {
        return this.repository.getPanelData().pipe(map((response) => {
            return PanelPageModel.fromApiResponse(response.data);
        }), catchError(error => {
            throw error;
        }));
    }
};
PanelService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [PANEL_REPOSITORY,] }] }
];
PanelService.ɵprov = i0.ɵɵdefineInjectable({ factory: function PanelService_Factory() { return new PanelService(i0.ɵɵinject(i1.PANEL_REPOSITORY)); }, token: PanelService, providedIn: "root" });
PanelService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(0, Inject(PANEL_REPOSITORY))
], PanelService);
export { PanelService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGFuZWwuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3BhbmVsLWNvcmUvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvUGFuZWwuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHbkQsT0FBTyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDdkQsT0FBTyxFQUF1QyxnQkFBZ0IsRUFBRSxNQUFNLG1DQUFtQyxDQUFDOzs7QUFPMUcsSUFBYSxZQUFZLEdBQXpCLE1BQWEsWUFBWTtJQUNyQixZQUNzQyxVQUE0QjtRQUE1QixlQUFVLEdBQVYsVUFBVSxDQUFrQjtJQUM5RCxDQUFDO0lBRUwsWUFBWTtRQUNSLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxJQUFJLENBQ3RDLEdBQUcsQ0FBQyxDQUFDLFFBQStDLEVBQUUsRUFBRTtZQUVwRCxPQUFPLGNBQWMsQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pELENBQUMsQ0FBQyxFQUNGLFVBQVUsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUNmLE1BQU0sS0FBSyxDQUFDO1FBQ2hCLENBQUMsQ0FBQyxDQUNMLENBQUM7SUFDTixDQUFDO0NBQ0osQ0FBQTs7NENBZFEsTUFBTSxTQUFDLGdCQUFnQjs7O0FBRm5CLFlBQVk7SUFIeEIsVUFBVSxDQUFDO1FBQ1IsVUFBVSxFQUFFLE1BQU07S0FDckIsQ0FBQztJQUdPLFdBQUEsTUFBTSxDQUFDLGdCQUFnQixDQUFDLENBQUE7R0FGcEIsWUFBWSxDQWdCeEI7U0FoQlksWUFBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSUh0dHBCYXNpY1Jlc3BvbnNlIH0gZnJvbSAnQGJveHgvY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBjYXRjaEVycm9yLCBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBQYW5lbFBhZ2VNb2RlbCB9IGZyb20gJy4uL21vZGVscy9wYW5lbC5tb2RlbCc7XG5pbXBvcnQgeyBJUGFuZWxBcGlSZXNwb25zZSwgSVBhbmVsUmVwb3NpdG9yeSwgUEFORUxfUkVQT1NJVE9SWSB9IGZyb20gJy4uL3JlcG9zaXRvcmllcy9JUGFuZWwucmVwb3NpdG9yeSc7XG5pbXBvcnQgeyBJUGFuZWxTZXJ2aWNlIH0gZnJvbSAnLi9JUGFuZWwuc2VydmljZSc7XG5cblxuQEluamVjdGFibGUoe1xuICAgIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBQYW5lbFNlcnZpY2UgaW1wbGVtZW50cyBJUGFuZWxTZXJ2aWNlIHtcbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgQEluamVjdChQQU5FTF9SRVBPU0lUT1JZKSBwcml2YXRlIHJlcG9zaXRvcnk6IElQYW5lbFJlcG9zaXRvcnlcbiAgICApIHsgfVxuXG4gICAgZ2V0UGFuZWxEYXRhKCk6IE9ic2VydmFibGU8UGFuZWxQYWdlTW9kZWw+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMucmVwb3NpdG9yeS5nZXRQYW5lbERhdGEoKS5waXBlKFxuICAgICAgICAgICAgbWFwKChyZXNwb25zZTogSUh0dHBCYXNpY1Jlc3BvbnNlPElQYW5lbEFwaVJlc3BvbnNlPikgPT4ge1xuXG4gICAgICAgICAgICAgICAgcmV0dXJuIFBhbmVsUGFnZU1vZGVsLmZyb21BcGlSZXNwb25zZShyZXNwb25zZS5kYXRhKTtcbiAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgY2F0Y2hFcnJvcihlcnJvciA9PiB7XG4gICAgICAgICAgICAgICAgdGhyb3cgZXJyb3I7XG4gICAgICAgICAgICB9KVxuICAgICAgICApO1xuICAgIH1cbn1cbiJdfQ==