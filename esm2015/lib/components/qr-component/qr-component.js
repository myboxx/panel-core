import { __awaiter, __decorate } from "tslib";
import { Component, Input } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { Subject } from 'rxjs';
import { QrPopoverComponent } from './qr-popover.component';
let QrComponent = class QrComponent {
    constructor(popoverController) {
        this.popoverController = popoverController;
        this.destroyed$ = new Subject();
    }
    ngOnInit() {
    }
    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }
    openQRPopover() {
        return __awaiter(this, void 0, void 0, function* () {
            const popover = yield this.popoverController.create({
                component: QrPopoverComponent,
                componentProps: {
                    websiteUrlString: this.websiteUrl
                }
            });
            yield popover.present();
        });
    }
};
QrComponent.ctorParameters = () => [
    { type: PopoverController }
];
__decorate([
    Input()
], QrComponent.prototype, "isLoading$", void 0);
__decorate([
    Input()
], QrComponent.prototype, "websiteUrl", void 0);
QrComponent = __decorate([
    Component({
        selector: 'boxx-qr-component',
        template: "<div id=\"qr-spinner-container\" *ngIf=\"(isLoading$ | async) === true\">\n    <ion-spinner  id=\"qr-spinner\"></ion-spinner>\n</div>\n<div *ngIf=\"websiteUrl && (isLoading$ | async) === false\">\n    <ion-icon src=\"assets/icon/qr.svg\" slot=\"end\" (click)=\"openQRPopover()\">\n    </ion-icon>\n</div>\n",
        styles: ["#qr-spinner-container{width:100%;text-align:center}#qr-spinner-container ion-spinner{color:var(--ion-color-primary-contrast,#fff)}ion-icon{border-radius:5px;padding:5px;background:var(--ion-color-primary-contrast,#fff)}"]
    })
], QrComponent);
export { QrComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicXItY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvcGFuZWwtY29yZS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL3FyLWNvbXBvbmVudC9xci1jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFxQixNQUFNLGVBQWUsQ0FBQztBQUNwRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNuRCxPQUFPLEVBQWMsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBTzVELElBQWEsV0FBVyxHQUF4QixNQUFhLFdBQVc7SUFJcEIsWUFDWSxpQkFBb0M7UUFBcEMsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUloRCxlQUFVLEdBQUcsSUFBSSxPQUFPLEVBQVcsQ0FBQztJQUhoQyxDQUFDO0lBS0wsUUFBUTtJQUVSLENBQUM7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBRUssYUFBYTs7WUFDZixNQUFNLE9BQU8sR0FBRyxNQUFNLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUM7Z0JBQ2hELFNBQVMsRUFBRSxrQkFBa0I7Z0JBQzdCLGNBQWMsRUFBRTtvQkFDWixnQkFBZ0IsRUFBRSxJQUFJLENBQUMsVUFBVTtpQkFDcEM7YUFDSixDQUFDLENBQUM7WUFDSCxNQUFNLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUM1QixDQUFDO0tBQUE7Q0FDSixDQUFBOztZQXhCa0MsaUJBQWlCOztBQUp2QztJQUFSLEtBQUssRUFBRTsrQ0FBaUM7QUFDaEM7SUFBUixLQUFLLEVBQUU7K0NBQW9CO0FBRm5CLFdBQVc7SUFMdkIsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLG1CQUFtQjtRQUM3Qiw4VEFBa0M7O0tBRXJDLENBQUM7R0FDVyxXQUFXLENBNkJ2QjtTQTdCWSxXQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25EZXN0cm95LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFBvcG92ZXJDb250cm9sbGVyIH0gZnJvbSAnQGlvbmljL2FuZ3VsYXInO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgUXJQb3BvdmVyQ29tcG9uZW50IH0gZnJvbSAnLi9xci1wb3BvdmVyLmNvbXBvbmVudCc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYm94eC1xci1jb21wb25lbnQnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9xci1jb21wb25lbnQuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vcXItY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBRckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgICBASW5wdXQoKSBpc0xvYWRpbmckOiBPYnNlcnZhYmxlPGJvb2xlYW4+O1xuICAgIEBJbnB1dCgpIHdlYnNpdGVVcmw6IHN0cmluZztcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcml2YXRlIHBvcG92ZXJDb250cm9sbGVyOiBQb3BvdmVyQ29udHJvbGxlclxuICAgICkgeyB9XG5cblxuICAgIGRlc3Ryb3llZCQgPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xuXG4gICAgbmdPbkluaXQoKSB7XG5cbiAgICB9XG5cbiAgICBuZ09uRGVzdHJveSgpIHtcbiAgICAgICAgdGhpcy5kZXN0cm95ZWQkLm5leHQodHJ1ZSk7XG4gICAgICAgIHRoaXMuZGVzdHJveWVkJC5jb21wbGV0ZSgpO1xuICAgIH1cblxuICAgIGFzeW5jIG9wZW5RUlBvcG92ZXIoKSB7XG4gICAgICAgIGNvbnN0IHBvcG92ZXIgPSBhd2FpdCB0aGlzLnBvcG92ZXJDb250cm9sbGVyLmNyZWF0ZSh7XG4gICAgICAgICAgICBjb21wb25lbnQ6IFFyUG9wb3ZlckNvbXBvbmVudCxcbiAgICAgICAgICAgIGNvbXBvbmVudFByb3BzOiB7XG4gICAgICAgICAgICAgICAgd2Vic2l0ZVVybFN0cmluZzogdGhpcy53ZWJzaXRlVXJsXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICBhd2FpdCBwb3BvdmVyLnByZXNlbnQoKTtcbiAgICB9XG59XG4iXX0=