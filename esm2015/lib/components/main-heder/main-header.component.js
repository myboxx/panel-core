import { __decorate } from "tslib";
import { Component } from '@angular/core';
let MainHeaderComponent = class MainHeaderComponent {
    constructor() { }
    ngOnInit() { }
};
MainHeaderComponent = __decorate([
    Component({
        selector: 'boxx-main-header',
        template: "<div id=\"panel-header\">\n    <ion-row id=\"panel-header-info\">\n        <ion-col size=\"10\">\n            <ng-content select=\"ion-label\"></ng-content>\n        </ion-col>\n        <ion-col size=\"2\" id=\"header-right-col\">\n            <ng-content select=\"boxx-qr-component\"></ng-content>\n        </ion-col>\n    </ion-row>\n    <ng-content></ng-content>\n</div>",
        styles: ["#panel-header{position:relative;height:160px;border-bottom-left-radius:50% 50%;border-bottom-right-radius:50% 50%;width:120%;margin-top:-1px;margin-left:-10%;padding-left:10%;padding-right:10%;background-color:var(--ion-color-primary,#fff);color:var(--ion-color-primary-contrast,#000);border-left:6px solid var(--ion-color-secondary,#000);border-bottom:6px solid var(--ion-color-secondary,#000);border-right:6px solid var(--ion-color-secondary,#000)}#panel-header-info{font-size:12px;padding:8px}#panel-header-info ion-col{padding:0}#header-right-col{padding:1px;font-size:2.7em;text-align:right}"]
    })
], MainHeaderComponent);
export { MainHeaderComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi1oZWFkZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvcGFuZWwtY29yZS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL21haW4taGVkZXIvbWFpbi1oZWFkZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBT2xELElBQWEsbUJBQW1CLEdBQWhDLE1BQWEsbUJBQW1CO0lBRTlCLGdCQUFnQixDQUFDO0lBRWpCLFFBQVEsS0FBSSxDQUFDO0NBRWQsQ0FBQTtBQU5ZLG1CQUFtQjtJQUwvQixTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsa0JBQWtCO1FBQzVCLGlZQUEyQzs7S0FFNUMsQ0FBQztHQUNXLG1CQUFtQixDQU0vQjtTQU5ZLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2JveHgtbWFpbi1oZWFkZXInLFxuICB0ZW1wbGF0ZVVybDogJy4vbWFpbi1oZWFkZXIuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9tYWluLWhlYWRlci5jb21wb25lbnQuc2NzcyddLFxufSlcbmV4cG9ydCBjbGFzcyBNYWluSGVhZGVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIG5nT25Jbml0KCkge31cblxufVxuIl19