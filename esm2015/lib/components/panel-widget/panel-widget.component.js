import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
let PanelWidgetComponent = class PanelWidgetComponent {
    constructor() { }
    ngOnInit() { }
};
__decorate([
    Input()
], PanelWidgetComponent.prototype, "title", void 0);
PanelWidgetComponent = __decorate([
    Component({
        selector: 'boxx-panel-widget',
        template: "<div class=\"boxx-panel-widget\">\n    <ion-item lines=\"none\" [hidden]=\"!title\">\n        <ion-label class=\"widget-title\">{{title}}</ion-label>\n\n        <ion-note slot=\"end\">\n            <ng-content select=\"div[forwardLink]\"></ng-content>\n        </ion-note>\n    </ion-item>\n\n    <ng-content></ng-content>\n</div>",
        styles: [":host .boxx-panel-widget{margin-bottom:16px}:host .widget-title{font-size:larger;font-weight:600}"]
    })
], PanelWidgetComponent);
export { PanelWidgetComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWwtd2lkZ2V0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3BhbmVsLWNvcmUvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9wYW5lbC13aWRnZXQvcGFuZWwtd2lkZ2V0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFPekQsSUFBYSxvQkFBb0IsR0FBakMsTUFBYSxvQkFBb0I7SUFHN0IsZ0JBQWdCLENBQUM7SUFFakIsUUFBUSxLQUFLLENBQUM7Q0FFakIsQ0FBQTtBQU5ZO0lBQVIsS0FBSyxFQUFFO21EQUFlO0FBRGQsb0JBQW9CO0lBTGhDLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxtQkFBbUI7UUFDN0Isc1ZBQTRDOztLQUUvQyxDQUFDO0dBQ1csb0JBQW9CLENBT2hDO1NBUFksb0JBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYm94eC1wYW5lbC13aWRnZXQnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9wYW5lbC13aWRnZXQuY29tcG9uZW50Lmh0bWwnLFxuICAgIHN0eWxlVXJsczogWycuL3BhbmVsLXdpZGdldC5jb21wb25lbnQuc2NzcyddLFxufSlcbmV4cG9ydCBjbGFzcyBQYW5lbFdpZGdldENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgQElucHV0KCkgdGl0bGU6IHN0cmluZztcblxuICAgIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgICBuZ09uSW5pdCgpIHsgfVxuXG59XG4iXX0=