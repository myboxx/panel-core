import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
let KpiCellComponent = class KpiCellComponent {
    constructor() { }
    ngOnInit() { }
};
__decorate([
    Input()
], KpiCellComponent.prototype, "className", void 0);
__decorate([
    Input()
], KpiCellComponent.prototype, "iconName", void 0);
__decorate([
    Input()
], KpiCellComponent.prototype, "iconSrc", void 0);
__decorate([
    Input()
], KpiCellComponent.prototype, "label", void 0);
__decorate([
    Input()
], KpiCellComponent.prototype, "value", void 0);
KpiCellComponent = __decorate([
    Component({
        selector: 'boxx-kpi-cell',
        template: "<!-- KPI CELL -->\n<div class=\"kpi {{className}}\">\n    <div class=\"kpi-icon\">\n        <ion-icon name=\"{{iconName}}\" src=\"{{iconSrc}}\" [hidden]=\"!iconName && !iconSrc\"></ion-icon>\n    </div>\n    <div class=\"kpi-label\">\n        {{label}}\n    </div>\n    <div class=\"kpi-counter\">\n        <div class=\"value\">{{value}}</div>\n    </div>\n</div>",
        styles: [":host{display:contents}:host .kpi{text-align:center;min-width:120px;max-width:100%;min-height:125px;max-height:300px;padding:10px;box-shadow:#d3d3d3 0 0 4px;border-radius:29px;background-color:#fff;position:relative;display:-ms-grid;display:grid}:host .kpi ion-icon{font-size:x-large;display:flex;margin:auto}"]
    })
], KpiCellComponent);
export { KpiCellComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoia3BpLWNlbGwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvcGFuZWwtY29yZS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2twaS1jZWxsL2twaS1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFPekQsSUFBYSxnQkFBZ0IsR0FBN0IsTUFBYSxnQkFBZ0I7SUFPM0IsZ0JBQWdCLENBQUM7SUFFakIsUUFBUSxLQUFJLENBQUM7Q0FFZCxDQUFBO0FBVlU7SUFBUixLQUFLLEVBQUU7bURBQW1CO0FBQ2xCO0lBQVIsS0FBSyxFQUFFO2tEQUFrQjtBQUNqQjtJQUFSLEtBQUssRUFBRTtpREFBaUI7QUFDaEI7SUFBUixLQUFLLEVBQUU7K0NBQWU7QUFDZDtJQUFSLEtBQUssRUFBRTsrQ0FBZTtBQUxaLGdCQUFnQjtJQUw1QixTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsZUFBZTtRQUN6Qix1WEFBd0M7O0tBRXpDLENBQUM7R0FDVyxnQkFBZ0IsQ0FXNUI7U0FYWSxnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYm94eC1rcGktY2VsbCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9rcGktY2VsbC5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2twaS1jZWxsLmNvbXBvbmVudC5zY3NzJ10sXG59KVxuZXhwb3J0IGNsYXNzIEtwaUNlbGxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBASW5wdXQoKSBjbGFzc05hbWU6IHN0cmluZztcbiAgQElucHV0KCkgaWNvbk5hbWU6IHN0cmluZztcbiAgQElucHV0KCkgaWNvblNyYzogc3RyaW5nO1xuICBASW5wdXQoKSBsYWJlbDogc3RyaW5nO1xuICBASW5wdXQoKSB2YWx1ZTogbnVtYmVyO1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7fVxuXG59XG4iXX0=